package org.green.cassandra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CassandraCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(CassandraCoreApplication.class, args);
	}

}
