package org.green.cassandra.entity;

import org.green.cassandra.entity.generic.AbstractTableType1;
import org.springframework.data.cassandra.core.mapping.Table;


@Table(value = "AC_CGI_Activity", forceQuote = true)
public class AcCgiActivityEntity extends AbstractTableType1 {
}
