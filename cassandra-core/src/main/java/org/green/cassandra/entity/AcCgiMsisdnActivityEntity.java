package org.green.cassandra.entity;

import org.green.cassandra.entity.generic.AbstractKey3TableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "AC_CGI_MsisdnActivity", forceQuote = true)
public class AcCgiMsisdnActivityEntity extends AbstractKey3TableType {
}
