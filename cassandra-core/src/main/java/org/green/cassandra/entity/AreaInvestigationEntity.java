package org.green.cassandra.entity;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "area_investigations", forceQuote = true)
public class AreaInvestigationEntity extends AbstractTableType<String> {
}
