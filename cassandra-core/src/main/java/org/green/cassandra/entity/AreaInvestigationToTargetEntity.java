package org.green.cassandra.entity;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.springframework.data.cassandra.core.mapping.Table;


@Table(value = "area_investigation_to_target", forceQuote = true)
public class AreaInvestigationToTargetEntity extends AbstractTableType<String> {
}
