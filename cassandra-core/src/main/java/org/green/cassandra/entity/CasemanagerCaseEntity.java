package org.green.cassandra.entity;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "Casemanager_Case", forceQuote = true)
public class CasemanagerCaseEntity extends AbstractTableType<String> {
}
