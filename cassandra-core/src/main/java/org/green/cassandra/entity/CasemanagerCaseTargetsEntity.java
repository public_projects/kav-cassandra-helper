package org.green.cassandra.entity;

import org.green.cassandra.entity.generic.AbstractKey2TableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "Casemanager_Case_Targets", forceQuote = true)
public class CasemanagerCaseTargetsEntity extends AbstractKey2TableType {
}
