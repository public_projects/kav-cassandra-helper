package org.green.cassandra.entity;

import org.green.cassandra.entity.generic.AbstractKey2TableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "Casemanager_Target_Cases", forceQuote = true)
public class CasemanagerTargetCasesEntity extends AbstractKey2TableType {

}
