package org.green.cassandra.entity;

import org.green.cassandra.entity.generic.AbstractColumn2TableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "Geo_IdentityMovementActivity", forceQuote = true)
public class GeoIdentityMovementActivityEntity extends AbstractColumn2TableType {}
