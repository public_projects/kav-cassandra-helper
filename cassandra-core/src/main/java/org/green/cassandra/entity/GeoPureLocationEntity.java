package org.green.cassandra.entity;


import org.green.cassandra.entity.generic.AbstractKey3TableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "Geo_PureLocations", forceQuote = true)
public class GeoPureLocationEntity extends AbstractKey3TableType {
}
