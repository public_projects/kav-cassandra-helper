package org.green.cassandra.entity;

import org.green.cassandra.entity.generic.AbstractKey3TableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "TM_Identity_Origin_Info", forceQuote = true)
public class IdentityOriginInfoEntity extends AbstractKey3TableType {
}
