package org.green.cassandra.entity;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "AC_ImeiTypeAllocationCode", forceQuote = true)
public class ImeiTypeAllocationCodeEntity extends AbstractTableType<String> {
}
