package org.green.cassandra.entity;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "intercept_content_locator", forceQuote = true)
public class InterceptContentLocatorEntity extends AbstractTableType<String> {
}
