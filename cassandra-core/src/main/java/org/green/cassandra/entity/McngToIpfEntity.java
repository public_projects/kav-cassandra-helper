package org.green.cassandra.entity;

import org.green.cassandra.entity.generic.AbstractKey3TableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "TP_Mcng_To_Ipf_v2", forceQuote = true)
public class McngToIpfEntity extends AbstractKey3TableType {
}
