package org.green.cassandra.entity;

import org.green.cassandra.entity.generic.AbstractKey2TableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "TP_Msisdn_Targets", forceQuote = true)
public class TpMsisdnTargetEntity extends AbstractKey2TableType {
}
