package org.green.cassandra.entity;

import org.green.cassandra.entity.generic.AbstractKey2TableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "TP_Target_Msisdn", forceQuote = true)
public class TpTargetMsisdnEntity extends AbstractKey2TableType {
}
