package org.green.cassandra.entity.analysis;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;

@Table(value = "ImportProcessMetadata", forceQuote = true)
public class ImportProcessMetadataEntity extends AbstractTableType<String> implements Serializable {
}
