package org.green.cassandra.entity.analysis;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "InvestigationManagement_v2", forceQuote = true)
public class InvestigationManagementEntity extends AbstractTableType<String> implements Comparable<InvestigationManagementEntity> {

    @Override
    public int compareTo(InvestigationManagementEntity o) {
        if (Long.parseLong(o.getColumn1()) > Long.parseLong(getColumn1())) {
            return 1;
        }
        else if (Long.parseLong(o.getColumn1()) < Long.parseLong(getColumn1())) {
            return -1;
        }
        return 0;
    }
}
