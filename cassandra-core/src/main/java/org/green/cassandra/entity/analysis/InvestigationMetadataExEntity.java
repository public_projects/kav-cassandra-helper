package org.green.cassandra.entity.analysis;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;

@Table(value = "InvestigationMetadataEx", forceQuote = true)
public class InvestigationMetadataExEntity extends AbstractTableType<String> implements Serializable {
}
