package org.green.cassandra.entity.analysis;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;

@Table(value = "ObservationMetadata_v2", forceQuote = true)
public class ObservationMetadataEntity extends AbstractTableType<String> implements Serializable {
}
