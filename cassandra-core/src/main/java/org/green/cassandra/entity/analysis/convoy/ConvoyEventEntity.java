package org.green.cassandra.entity.analysis.convoy;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.springframework.data.cassandra.core.mapping.Table;


@Table(value = "ConvoyEvent", forceQuote = true)
public class ConvoyEventEntity extends AbstractTableType {
}
