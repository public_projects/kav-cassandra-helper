package org.green.cassandra.entity.analysis.convoy;

import org.green.cassandra.entity.generic.AbstractTableType6;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "ConvoyAnalysis_ObservationEvent", forceQuote = true)
public class ObservationEventEntity extends AbstractTableType6 {}
