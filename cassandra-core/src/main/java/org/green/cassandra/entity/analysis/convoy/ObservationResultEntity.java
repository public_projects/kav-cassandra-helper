package org.green.cassandra.entity.analysis.convoy;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "ObservationResult", forceQuote = true)
public class ObservationResultEntity extends AbstractTableType<String> {}
