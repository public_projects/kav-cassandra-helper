package org.green.cassandra.entity.analysis.convoy;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "ConvoyAnalysis_ObservationSubject", forceQuote = true)
public class ObservationSubjectEntity extends AbstractTableType<String> {}
