package org.green.cassandra.entity.analysis.eba;

import org.green.cassandra.entity.generic.AbstractKey7TableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "ObservationParticipationOthers", forceQuote = true)
public class ObservationParticipationOthersEntity extends AbstractKey7TableType {

    public static String getHeaders() {
        return "key" + CSV_DELIMETER +
                "key2" + CSV_DELIMETER +
                "key3" + CSV_DELIMETER +
                "key4" + CSV_DELIMETER +
                "column1" + CSV_DELIMETER +
                "column2" + CSV_DELIMETER +
                "value" + System.lineSeparator();
    }

    @Override
    public String toString() {
        return getKey() + "," + getKey2() + "," + getKey3() + "," + getKey4() + "," + getColumn1() + "," + getColumn2() + "," + getValue();
    }
}
