package org.green.cassandra.entity.data.request;

import org.green.cassandra.entity.generic.AbstractKey1;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "AC_CdrAdHocRequestHistory", forceQuote = true)
public class AcCdrAdHocRequestHistoryEntity extends AbstractKey1<Long> {
    @Column("column1")
    private long column1;
    @Column("column2")
    private String column2;
    @Column("value")
    private String value;

    public long getColumn1() {
        return column1;
    }

    public void setColumn1(long column1) {
        this.column1 = column1;
    }

    public String getColumn2() {
        return column2;
    }

    public void setColumn2(String column2) {
        this.column2 = column2;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
