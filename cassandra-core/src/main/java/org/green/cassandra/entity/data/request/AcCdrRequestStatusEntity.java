package org.green.cassandra.entity.data.request;

import org.green.cassandra.entity.generic.AbstractTableType10;
import org.springframework.data.cassandra.core.mapping.Table;


@Table(value = "AC_CdrRequestStatus", forceQuote = true)
public class AcCdrRequestStatusEntity extends AbstractTableType10 {
}
