package org.green.cassandra.entity.file;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "FileMetadata", forceQuote = true)
public class FileMetadataEntity extends AbstractTableType<String> {
}
