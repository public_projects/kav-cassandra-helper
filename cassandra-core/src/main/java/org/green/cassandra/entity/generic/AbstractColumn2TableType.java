package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.mapping.Column;

public class AbstractColumn2TableType extends AbstractKey2TableType {
    @Column("column2")
    private String column2;

    public String getColumn2() {
        return column2;
    }

    public void setColumn2(String column2) {
        this.column2 = column2;
    }
}
