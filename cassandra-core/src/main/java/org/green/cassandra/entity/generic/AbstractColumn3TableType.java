package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.mapping.Column;

public class AbstractColumn3TableType extends AbstractColumn2TableType {
    @Column("column3")
    protected String column3;

    public String getColumn3() {
        return column3;
    }

    public void setColumn3(String column3) {
        this.column3 = column3;
    }
}
