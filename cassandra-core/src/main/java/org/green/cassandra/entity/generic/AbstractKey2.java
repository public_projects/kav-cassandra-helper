package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

public abstract class AbstractKey2<T> extends AbstractKey1<String> {
    @PrimaryKeyColumn(name = "key2", ordinal = 1, type = PrimaryKeyType.CLUSTERED)
    private Long key2;

    public Long getKey2() {
        return key2;
    }

    public void setKey2(Long key2) {
        this.key2 = key2;
    }

    public String getHeader() {
        return super.getHeader() + CSV_DELIMETER + "key2";
    }

    @Override
    public String toString() {
        return super.toString() + CSV_DELIMETER + key2;
    }
}
