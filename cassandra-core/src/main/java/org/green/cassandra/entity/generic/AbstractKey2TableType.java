package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

public abstract class AbstractKey2TableType extends AbstractTableType<String> {
        @PrimaryKeyColumn(name = "key2", ordinal = 1, type = PrimaryKeyType.CLUSTERED)
        private String key2;

        public String getKey2() {
                return key2;
        }

        public void setKey2(String key2) {
                this.key2 = key2;
        }
}
