package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

public abstract class AbstractKey3<T> extends AbstractKey2<Long> {
    @PrimaryKeyColumn(name = "key3", ordinal = 2, type = PrimaryKeyType.CLUSTERED)
    private T key3;

    public T getKey3() {
        return key3;
    }

    public void setKey3(T key3) {
        this.key3 = key3;
    }

    public String getHeader() {
        return super.getHeader() + CSV_DELIMETER + "key3";
    }

    @Override
    public String toString() {
        return super.toString() + CSV_DELIMETER + key3;
    }
}
