package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;


public abstract class AbstractKey3TableType extends AbstractKey2TableType {
        @PrimaryKeyColumn(name = "key3", ordinal = 2, type = PrimaryKeyType.CLUSTERED)
        private String key3;

        public String getKey3() {
                return key3;
        }

        public void setKey3(String key3) {
                this.key3 = key3;
        }
}
