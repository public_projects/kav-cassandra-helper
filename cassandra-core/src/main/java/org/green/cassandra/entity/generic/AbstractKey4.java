package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

public abstract class AbstractKey4<T> extends AbstractKey3<Long> {
    @PrimaryKeyColumn(name = "key4", ordinal = 3, type = PrimaryKeyType.CLUSTERED)
    private T key4;

    public T getKey4() {
        return key4;
    }

    public void setKey4(T key4) {
        this.key4 = key4;
    }

    public String getHeader() {
        return super.getHeader() + CSV_DELIMETER + "key4";
    }

    @Override
    public String toString() {
        return super.toString() + CSV_DELIMETER + key4;
    }
}
