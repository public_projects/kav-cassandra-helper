package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

public class AbstractKey4TableType extends AbstractKey3TableType {
    @PrimaryKeyColumn(name = "key4", ordinal = 2, type = PrimaryKeyType.CLUSTERED)
    private String key4;

    public String getKey4() {
        return key4;
    }

    public void setKey4(String key4) {
        this.key4 = key4;
    }
}
