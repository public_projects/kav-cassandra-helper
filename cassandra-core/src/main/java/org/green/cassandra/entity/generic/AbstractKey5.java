package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

public abstract class AbstractKey5<T> extends AbstractKey4<String> {
    @PrimaryKeyColumn(name = "key5", ordinal = 4, type = PrimaryKeyType.CLUSTERED)
    private T key5;

    public T getKey5() {
        return key5;
    }

    public void setKey5(T key5) {
        this.key5 = key5;
    }

    public String getHeader() {
        return super.getHeader() + CSV_DELIMETER + "key5";
    }

    @Override
    public String toString() {
        return super.toString() + CSV_DELIMETER + key5;
    }
}
