package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

public abstract class AbstractKey7TableType extends AbstractKey3TableType {
        @PrimaryKeyColumn(name = "key4", ordinal = 0, type = PrimaryKeyType.CLUSTERED)
        private String key4;
        @Column("column2")
        private String column2;

        public String getKey4() {
                return key4;
        }

        public void setKey4(String key4) {
                this.key4 = key4;
        }

        public String getColumn2() {
                return column2;
        }

        public void setColumn2(String column2) {
                this.column2 = column2;
        }
}
