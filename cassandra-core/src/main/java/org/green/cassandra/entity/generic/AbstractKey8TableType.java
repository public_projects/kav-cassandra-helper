package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.mapping.Column;

public abstract class AbstractKey8TableType extends AbstractKey3TableType {
    @Column("column2")
    private String column2;
    @Column("column3")
    private String column3;
    @Column("column4")
    private String column4;

    public String getColumn2() {
        return column2;
    }

    public void setColumn2(String column2) {
        this.column2 = column2;
    }

    public String getColumn3() {
        return column3;
    }

    public void setColumn3(String column3) {
        this.column3 = column3;
    }

    public String getColumn4() {
        return column4;
    }

    public void setColumn4(String column4) {
        this.column4 = column4;
    }
}
