package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import java.io.Serializable;

public abstract class AbstractTableType<T> implements Serializable {
    public static final String CSV_DELIMETER = ";";

    public static String getHeader() {
        return "key" + CSV_DELIMETER + "column1" + CSV_DELIMETER + "value" + System.lineSeparator();
    }

    @PrimaryKeyColumn(name = "key", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private T key;
    @Column("column1")
    private String column1;
    @Column("value")
    private String value;

    public T getKey() {
        return key;
    }

    public void setKey(T key) {
        this.key = key;
    }

    public String getColumn1() {
        return column1;
    }

    public void setColumn1(String column1) {
        this.column1 = column1;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return key + CSV_DELIMETER + column1 + CSV_DELIMETER + value + System.lineSeparator();
    }
}
