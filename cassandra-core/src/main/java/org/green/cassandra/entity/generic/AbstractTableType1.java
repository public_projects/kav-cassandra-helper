package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

public abstract class AbstractTableType1 extends AbstractTableType<String> {
        @PrimaryKeyColumn(name = "key2", ordinal = 1, type = PrimaryKeyType.CLUSTERED)
        private String key2;
        @Column("column2")
        private String column2;

        public String getKey2() {
                return key2;
        }

        public void setKey2(String key2) {
                this.key2 = key2;
        }

        public String getColumn2() {
                return column2;
        }

        public void setColumn2(String column2) {
                this.column2 = column2;
        }
}
