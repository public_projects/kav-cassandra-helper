package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.mapping.Column;

public class AbstractTableType10 extends AbstractKey5<String> {
    @Column("column1")
    private String column1;
    @Column("value")
    private String value;

    public String getHeader() {
        return CSV_DELIMETER +"column1" + CSV_DELIMETER + "value";
    }

    public String getColumn1() {
        return column1;
    }

    public void setColumn1(String column1) {
        this.column1 = column1;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return super.toString() + CSV_DELIMETER + column1 + CSV_DELIMETER + value;
    }
}
