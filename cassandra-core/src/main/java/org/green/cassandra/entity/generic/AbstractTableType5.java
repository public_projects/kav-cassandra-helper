package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.mapping.Column;


public class AbstractTableType5 extends AbstractTableType<String> {
    @Column("column2")
    protected String column2;

    public String getColumn2() {
        return column2;
    }

    public void setColumn2(String column2) {
        this.column2 = column2;
    }
}
