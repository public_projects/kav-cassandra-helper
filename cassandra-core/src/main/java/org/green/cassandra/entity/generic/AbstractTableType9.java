package org.green.cassandra.entity.generic;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import java.io.Serializable;

public abstract class AbstractTableType9 implements Serializable {
    public static final String CSV_DELIMETER = ";";

    public static String getHeader() {
        return "key" + CSV_DELIMETER + "column1" + CSV_DELIMETER + "value" + System.lineSeparator();
    }

    @PrimaryKeyColumn(name = "key", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private String key;
    @Column("column1")
    private Long column1;
    @Column("value")
    private String value;
    @Override
    public String toString() {
        return key + CSV_DELIMETER + column1 + CSV_DELIMETER + value + System.lineSeparator();
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getColumn1() {
        return column1;
    }

    public void setColumn1(Long column1) {
        this.column1 = column1;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
