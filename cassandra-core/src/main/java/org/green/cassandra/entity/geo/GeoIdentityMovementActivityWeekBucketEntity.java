package org.green.cassandra.entity.geo;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "Geo_IdentityMovementActivity_WeekBucket", forceQuote = true)
public class GeoIdentityMovementActivityWeekBucketEntity extends AbstractTableType<String> {
}
