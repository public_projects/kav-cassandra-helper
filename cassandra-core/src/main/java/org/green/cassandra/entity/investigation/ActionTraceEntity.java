package org.green.cassandra.entity.investigation;

import org.green.cassandra.entity.generic.AbstractTableType5;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "ActionTrace", forceQuote = true)
public class ActionTraceEntity extends AbstractTableType5 {
}
