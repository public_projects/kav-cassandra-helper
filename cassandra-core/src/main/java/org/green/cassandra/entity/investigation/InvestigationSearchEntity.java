package org.green.cassandra.entity.investigation;

import org.green.cassandra.entity.generic.AbstractTableType5;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "InvestigationSearch", forceQuote = true)
public class InvestigationSearchEntity extends AbstractTableType5 {
}
