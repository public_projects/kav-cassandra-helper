package org.green.cassandra.entity.proximity_analysis;

import org.green.cassandra.entity.generic.AbstractKey4TableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "ProximityAnalysis_IdentityLocation", forceQuote = true)
public class ProximityAnalysisIdentityLocationEntity extends AbstractKey4TableType {

    public String toString() {
        return getKey() + CSV_DELIMETER +
                getKey2() + CSV_DELIMETER +
                getKey3() + CSV_DELIMETER +
                getKey4() + CSV_DELIMETER +
                getColumn1() + CSV_DELIMETER +
                getValue() + System.lineSeparator();
    }
}
