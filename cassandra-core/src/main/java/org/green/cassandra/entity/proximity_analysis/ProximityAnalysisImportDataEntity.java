package org.green.cassandra.entity.proximity_analysis;

import org.green.cassandra.entity.generic.AbstractKey8TableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "ProximityAnalysis_Import_Data", forceQuote = true)
public class ProximityAnalysisImportDataEntity extends AbstractKey8TableType {
    public static String getHeaders() {
        return "key " + CSV_DELIMETER + " key2" + CSV_DELIMETER + "key3" + CSV_DELIMETER + "column1" + CSV_DELIMETER + "column2" + CSV_DELIMETER + "column3" + CSV_DELIMETER + "column4" + CSV_DELIMETER + "value";
    }

    @Override
    public String toString() {
        return getKey() + CSV_DELIMETER  + getKey2() + CSV_DELIMETER  + getKey3() + CSV_DELIMETER  + getColumn1() + CSV_DELIMETER  + getColumn2() + CSV_DELIMETER  + getColumn3() + CSV_DELIMETER  + getColumn4() + CSV_DELIMETER  + getValue();
    }
}
