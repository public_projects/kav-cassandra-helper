package org.green.cassandra.entity.proximity_analysis;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "ProximityAnalysis_Investigation", forceQuote = true)
public class ProximityAnalysisInvestigationEntity extends AbstractTableType<String> {
    
}
