package org.green.cassandra.entity.proximity_analysis;

import org.green.cassandra.entity.generic.AbstractKey3TableType;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "ProximityAnalysis_InvestigationM", forceQuote = true)
public class ProximityAnalysisInvestigationMEntity extends AbstractKey3TableType {

}
