package org.green.cassandra.entity.proximity_analysis;

import org.green.cassandra.entity.generic.AbstractTableType9;
import org.springframework.data.cassandra.core.mapping.Table;


@Table(value = "ProximityAnalysis_InvestigationStatus", forceQuote = true)
public class ProximityAnalysisInvestigationStatusEntity extends AbstractTableType9 {

    public static String getHeaders() {
        return "key " + CSV_DELIMETER + " column1" + CSV_DELIMETER +  "value";
    }

    @Override
    public String toString() {
        return getKey() + CSV_DELIMETER  + getColumn1() + CSV_DELIMETER  + getValue();
    }
    
}
