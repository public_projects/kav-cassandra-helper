package org.green.cassandra.entity.proximity_analysis;

import org.green.cassandra.entity.generic.AbstractKey2TableType;
import org.springframework.data.cassandra.core.mapping.Table;


@Table(value = "ProximityAnalysis_ObservationI", forceQuote = true)
public class ProximityAnalysisObservationIEntity extends AbstractKey2TableType {
}
