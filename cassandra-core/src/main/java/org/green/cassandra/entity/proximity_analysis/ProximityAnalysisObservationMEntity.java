package org.green.cassandra.entity.proximity_analysis;

import org.green.cassandra.entity.generic.AbstractKey3TableType;
import org.springframework.data.cassandra.core.mapping.Table;


@Table(value = "ProximityAnalysis_ObservationM", forceQuote = true)
public class ProximityAnalysisObservationMEntity extends AbstractKey3TableType {
}
