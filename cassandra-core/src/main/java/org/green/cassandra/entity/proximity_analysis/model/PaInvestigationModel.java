package org.green.cassandra.entity.proximity_analysis.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaInvestigationModel implements Serializable {
    @JsonIgnore
    private String id;
    private String investigationName;
    private Long creationDate;

    public PaInvestigationModel(){}

    public PaInvestigationModel(String investigationName, Long creationDate) {
        this.investigationName = investigationName;
        this.creationDate = creationDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInvestigationName() {
        return investigationName;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public void setInvestigationName(String investigationName) {
        this.investigationName = investigationName;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

}
