package org.green.cassandra.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class ConvoyEventModel implements Serializable {
    private String id;
    private String name;
    private String position;
    private long callTime;
    private CellSpec cellSpec;
    private List<String> geoHashes;
    private Identity identity;
    private TimeInterval timeInterval;
    private Source source;
    private long createdDate;
    private long modifiedDate;

    // Add getters and setters for the above properties


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public long getCallTime() {
        return callTime;
    }

    public void setCallTime(long callTime) {
        this.callTime = callTime;
    }

    public CellSpec getCellSpec() {
        return cellSpec;
    }

    public void setCellSpec(CellSpec cellSpec) {
        this.cellSpec = cellSpec;
    }

    public List<String> getGeoHashes() {
        return geoHashes;
    }

    public void setGeoHashes(List<String> geoHashes) {
        this.geoHashes = geoHashes;
    }

    public Identity getIdentity() {
        return identity;
    }

    public void setIdentity(Identity identity) {
        this.identity = identity;
    }

    public TimeInterval getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(TimeInterval timeInterval) {
        this.timeInterval = timeInterval;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public long getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(long modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    // Nested classes for the inner JSON structures
    public static class CellSpec {
        private double latitude;
        private double longitude;
        private String cgi;
        private String locationProvider;
        private String locationSubject;

        // Add getters and setters


        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public String getCgi() {
            return cgi;
        }

        public void setCgi(String cgi) {
            this.cgi = cgi;
        }

        public String getLocationProvider() {
            return locationProvider;
        }

        public void setLocationProvider(String locationProvider) {
            this.locationProvider = locationProvider;
        }

        public String getLocationSubject() {
            return locationSubject;
        }

        public void setLocationSubject(String locationSubject) {
            this.locationSubject = locationSubject;
        }
    }

    public static class Identity {
        private Type type;
        private String id;

        // Add getters and setters

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class Type {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public static class TimeInterval {
        private long from;
        private long to;

        public long getFrom() {
            return from;
        }

        public void setFrom(long from) {
            this.from = from;
        }

        public long getTo() {
            return to;
        }

        public void setTo(long to) {
            this.to = to;
        }
        // Add getters and setters
    }

    public static class Source {
        private String system;
        private Properties properties;

        // Add getters and setters

        public String getSystem() {
            return system;
        }

        public void setSystem(String system) {
            this.system = system;
        }

        public Properties getProperties() {
            return properties;
        }

        public void setProperties(Properties properties) {
            this.properties = properties;
        }
    }

    public static class Properties {
        private String instance;
        @JsonProperty("agency-id")
        private String agencyId;
        @JsonProperty("agency-name")
        private String agencyName;

        // Add getters and setters

        public String getInstance() {
            return instance;
        }

        public void setInstance(String instance) {
            this.instance = instance;
        }

        public String getAgencyId() {
            return agencyId;
        }

        public void setAgencyId(String agencyId) {
            this.agencyId = agencyId;
        }

        public String getAgencyName() {
            return agencyName;
        }

        public void setAgencyName(String agencyName) {
            this.agencyName = agencyName;
        }
    }
}
