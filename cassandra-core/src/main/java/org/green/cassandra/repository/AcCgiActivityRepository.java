package org.green.cassandra.repository;

import org.green.cassandra.entity.AcCgiActivityEntity;
import org.green.cassandra.repository.generic.GenericRepository1;
import org.springframework.stereotype.Repository;

@Repository
public interface AcCgiActivityRepository extends GenericRepository1<AcCgiActivityEntity> {
}
