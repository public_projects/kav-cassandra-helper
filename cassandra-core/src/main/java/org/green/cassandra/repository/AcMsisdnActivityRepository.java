package org.green.cassandra.repository;

import org.green.cassandra.entity.AcMsisdnActivityEntity;
import org.green.cassandra.repository.generic.GenericRepository1;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AcMsisdnActivityRepository extends GenericRepository1<AcMsisdnActivityEntity> {
}

