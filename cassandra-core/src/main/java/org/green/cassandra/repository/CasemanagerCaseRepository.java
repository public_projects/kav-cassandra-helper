package org.green.cassandra.repository;

import org.green.cassandra.entity.CasemanagerCaseEntity;
import org.green.cassandra.repository.generic.GenericRepository;
import org.green.cassandra.repository.generic.GenericRepository1;
import org.green.cassandra.repository.generic.GenericRepository2;
import org.springframework.stereotype.Repository;


@Repository
public interface CasemanagerCaseRepository extends GenericRepository<CasemanagerCaseEntity> {

}
