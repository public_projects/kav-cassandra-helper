package org.green.cassandra.repository;

import org.green.cassandra.entity.CasemanagerCaseTargetsEntity;
import org.green.cassandra.repository.generic.GenericRepository1;
import org.green.cassandra.repository.generic.GenericRepository2;
import org.springframework.stereotype.Repository;


@Repository
public interface CasemanagerCaseTargetsRepository extends GenericRepository2<CasemanagerCaseTargetsEntity> {

}
