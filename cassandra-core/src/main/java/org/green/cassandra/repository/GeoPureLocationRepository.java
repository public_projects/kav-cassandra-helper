package org.green.cassandra.repository;

import org.green.cassandra.entity.GeoPureLocationEntity;
import org.green.cassandra.repository.generic.GenericRepository1;
import org.green.cassandra.repository.generic.GenericRepository3;
import org.springframework.stereotype.Repository;

@Repository
public interface GeoPureLocationRepository extends GenericRepository3<GeoPureLocationEntity> {}
