package org.green.cassandra.repository;

import org.green.cassandra.entity.IdentityOriginInfoEntity;
import org.green.cassandra.repository.generic.GenericRepository1;
import org.green.cassandra.repository.generic.GenericRepository3;
import org.springframework.stereotype.Repository;


@Repository
public interface IdentityOriginInfoEntityRepository extends GenericRepository3<IdentityOriginInfoEntity> {

}
