package org.green.cassandra.repository;

import org.green.cassandra.entity.ImeiTypeAllocationCodeEntity;
import org.green.cassandra.repository.generic.GenericRepository;

public interface ImeiTypeAllocationCodeRepository extends GenericRepository<ImeiTypeAllocationCodeEntity> {
}

