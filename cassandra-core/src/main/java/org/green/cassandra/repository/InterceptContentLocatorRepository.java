package org.green.cassandra.repository;

import org.green.cassandra.entity.InterceptContentLocatorEntity;
import org.green.cassandra.repository.generic.GenericRepository;

public interface InterceptContentLocatorRepository extends GenericRepository<InterceptContentLocatorEntity> {
}
