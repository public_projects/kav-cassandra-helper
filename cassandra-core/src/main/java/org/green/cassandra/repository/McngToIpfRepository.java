package org.green.cassandra.repository;

import org.green.cassandra.entity.McngToIpfEntity;
import org.green.cassandra.repository.generic.GenericRepository3;


public interface McngToIpfRepository extends GenericRepository3<McngToIpfEntity> {
}
