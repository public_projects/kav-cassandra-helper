package org.green.cassandra.repository;

import org.green.cassandra.entity.TpTargetEntity;
import org.green.cassandra.repository.generic.GenericRepository;
import org.green.cassandra.repository.generic.GenericRepository1;
import org.green.cassandra.repository.generic.GenericRepository2;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface TpTargetRepository extends CassandraRepository<TpTargetEntity, String> {
    List<TpTargetEntity> findByKey(String key);
}
