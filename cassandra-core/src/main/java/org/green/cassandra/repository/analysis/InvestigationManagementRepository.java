package org.green.cassandra.repository.analysis;

import org.green.cassandra.entity.analysis.InvestigationManagementEntity;
import org.green.cassandra.repository.generic.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvestigationManagementRepository extends GenericRepository<InvestigationManagementEntity> {}
