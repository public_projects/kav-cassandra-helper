package org.green.cassandra.repository.analysis;

import org.green.cassandra.entity.analysis.InvestigationMetadataExEntity;
import org.green.cassandra.repository.generic.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvestigationMetadataExRepository extends GenericRepository<InvestigationMetadataExEntity> {
}
