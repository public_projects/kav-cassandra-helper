package org.green.cassandra.repository.analysis;

import org.green.cassandra.entity.analysis.ObservationMetadataEntity;
import org.green.cassandra.repository.generic.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ObservationMetadataRepository extends GenericRepository<ObservationMetadataEntity> {
}
