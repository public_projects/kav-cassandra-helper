package org.green.cassandra.repository.analysis.convoy;


import org.green.cassandra.entity.analysis.convoy.ConvoyEventEntity;
import org.green.cassandra.repository.generic.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConvoyEventRepository extends GenericRepository<ConvoyEventEntity> {
}
