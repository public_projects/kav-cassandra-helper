package org.green.cassandra.repository.analysis.convoy;

import org.green.cassandra.entity.analysis.convoy.ObservationEventEntity;
import org.green.cassandra.repository.generic.GenericRepository6;
import org.springframework.stereotype.Repository;

@Repository
public interface ObservationEventRepository extends GenericRepository6<ObservationEventEntity> {}
