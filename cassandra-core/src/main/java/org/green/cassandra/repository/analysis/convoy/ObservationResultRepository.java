package org.green.cassandra.repository.analysis.convoy;

import org.green.cassandra.entity.analysis.convoy.ObservationResultEntity;
import org.green.cassandra.repository.generic.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ObservationResultRepository extends GenericRepository<ObservationResultEntity> {}
