package org.green.cassandra.repository.analysis.eba;

import org.green.cassandra.entity.analysis.eba.ObservationParticipationOthersEntity;
import org.green.cassandra.repository.generic.GenericRepository7;
import org.springframework.stereotype.Repository;

@Repository
public interface ObservationParticipationOthersRepository extends GenericRepository7<ObservationParticipationOthersEntity> {}
