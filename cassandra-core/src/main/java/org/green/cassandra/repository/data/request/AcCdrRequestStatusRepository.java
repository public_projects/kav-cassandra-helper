package org.green.cassandra.repository.data.request;

import org.green.cassandra.entity.data.request.AcCdrRequestStatusEntity;
import org.green.cassandra.repository.generic.GenericRepository11;
import org.springframework.stereotype.Repository;

@Repository
public interface AcCdrRequestStatusRepository extends GenericRepository11<AcCdrRequestStatusEntity> {
}
