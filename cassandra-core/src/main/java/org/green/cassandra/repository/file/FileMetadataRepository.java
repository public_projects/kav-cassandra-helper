package org.green.cassandra.repository.file;

import org.green.cassandra.entity.file.FileMetadataEntity;
import org.green.cassandra.repository.generic.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileMetadataRepository extends GenericRepository<FileMetadataEntity> {
}
