package org.green.cassandra.repository.generic;

import org.green.cassandra.entity.AcMsisdnActivityEntity;
import org.green.cassandra.entity.generic.AbstractTableType1;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;


@NoRepositoryBean
public interface GenericRepository1<T extends AbstractTableType1> extends CassandraRepository<T, String> {
    @AllowFiltering
    List<T> findByColumn1(String s);

    @AllowFiltering
    List<T> findByKey(String key);

    @AllowFiltering
    List<T> findByKeyAndKey2(String key, long key2);

    @AllowFiltering
    T findFirstByColumn1AndValue(String column1Content, String valueContent);

    @AllowFiltering
    List<T> findAllByKeyAndAndKey2In(String key, List<Long> key2Ls);

    // NOT Possible in Cassandra!
//    @AllowFiltering
//    List<T> findAllByKeyInAndAndKey2In(List<String> key1Ls, List<Long> key2Ls);
}
