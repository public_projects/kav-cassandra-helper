package org.green.cassandra.repository.generic;

import org.green.cassandra.entity.generic.AbstractTableType10;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface GenericRepository11<T extends AbstractTableType10> extends CassandraRepository<T, String> {
}
