package org.green.cassandra.repository.generic;

import org.green.cassandra.entity.generic.AbstractKey3TableType;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;


@NoRepositoryBean
public interface GenericRepository3<T extends AbstractKey3TableType> extends CassandraRepository<T, String> {
    @AllowFiltering
    List<T> findByColumn1(String s);

    @AllowFiltering
    List<T> findByKey(String key);
}
