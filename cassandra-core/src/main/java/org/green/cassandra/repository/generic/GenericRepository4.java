package org.green.cassandra.repository.generic;

import org.green.cassandra.entity.generic.AbstractColumn2TableType;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;


@NoRepositoryBean
public interface GenericRepository4<T extends AbstractColumn2TableType> extends CassandraRepository<T, String> {
    @AllowFiltering
    List<T> findByColumn1(String s);

    @AllowFiltering
    List<T> findByKey(String key);

    @AllowFiltering
    List<T> findByKeyAndKey2(String key, long key2);
}
