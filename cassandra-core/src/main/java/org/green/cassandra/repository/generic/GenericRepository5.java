package org.green.cassandra.repository.generic;

import org.green.cassandra.entity.generic.AbstractTableType5;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.repository.NoRepositoryBean;
import java.util.List;

@NoRepositoryBean
public interface GenericRepository5<T extends AbstractTableType5> extends CassandraRepository<T, String> {
    @AllowFiltering
    List<T> findByColumn1(String s);

    @AllowFiltering
    List<T> findByColumn2(String s);

    @AllowFiltering
    List<T> findByKey(String key);
}
