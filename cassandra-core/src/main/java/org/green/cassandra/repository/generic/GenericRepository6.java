package org.green.cassandra.repository.generic;

import org.green.cassandra.entity.generic.AbstractTableType6;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.repository.NoRepositoryBean;
import java.util.List;

@NoRepositoryBean
public interface GenericRepository6<T extends AbstractTableType6> extends CassandraRepository<T, String> {
    @AllowFiltering
    List<T> findByKeyAndKey2(String key, String key2);
}
