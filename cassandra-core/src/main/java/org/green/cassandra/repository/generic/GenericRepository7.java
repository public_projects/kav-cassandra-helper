package org.green.cassandra.repository.generic;

import org.green.cassandra.entity.generic.AbstractKey7TableType;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface GenericRepository7<T extends AbstractKey7TableType> extends CassandraRepository<T, String> {
}
