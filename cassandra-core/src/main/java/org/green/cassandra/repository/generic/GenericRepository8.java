package org.green.cassandra.repository.generic;

import org.green.cassandra.entity.generic.AbstractKey8TableType;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface GenericRepository8<T extends AbstractKey8TableType> extends CassandraRepository<T, String> {
}
