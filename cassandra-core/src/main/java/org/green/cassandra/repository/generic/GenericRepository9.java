package org.green.cassandra.repository.generic;

import org.green.cassandra.entity.generic.AbstractTableType;
import org.green.cassandra.entity.generic.AbstractTableType9;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;


@NoRepositoryBean
public interface GenericRepository9<T extends AbstractTableType9> extends CassandraRepository<T, String> {
    @AllowFiltering
    List<T> findByColumn1(Long s);

    @AllowFiltering
    List<T> findByKey(String key);

    @AllowFiltering
    T findFirstByColumn1AndValue(Long column1Content, String valueContent);

    @AllowFiltering
    T findFirstByKeyOrderByColumn1Desc(String key);
}
