package org.green.cassandra.repository.geo;

import org.green.cassandra.entity.GeoIdentityMovementActivityEntity;
import org.green.cassandra.repository.generic.GenericRepository4;
import org.springframework.stereotype.Repository;

@Repository
public interface GeoIdentityMovementActivityRepository extends GenericRepository4<GeoIdentityMovementActivityEntity> {
}
