package org.green.cassandra.repository.geo;

import org.green.cassandra.entity.geo.GeoIdentityMovementActivityWeekBucketEntity;
import org.green.cassandra.repository.generic.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GeoIdentityMovementActivityWeekBucketRepository extends GenericRepository<GeoIdentityMovementActivityWeekBucketEntity> {
}
