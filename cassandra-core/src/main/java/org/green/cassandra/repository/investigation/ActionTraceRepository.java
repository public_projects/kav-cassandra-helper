package org.green.cassandra.repository.investigation;

import org.green.cassandra.entity.investigation.ActionTraceEntity;
import org.green.cassandra.repository.generic.GenericRepository5;
import org.springframework.stereotype.Repository;

@Repository
public interface ActionTraceRepository extends GenericRepository5<ActionTraceEntity> {}
