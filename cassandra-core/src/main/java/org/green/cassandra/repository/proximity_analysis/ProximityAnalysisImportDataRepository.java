package org.green.cassandra.repository.proximity_analysis;

import org.green.cassandra.entity.proximity_analysis.ProximityAnalysisImportDataEntity;
import org.green.cassandra.repository.generic.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProximityAnalysisImportDataRepository extends GenericRepository<ProximityAnalysisImportDataEntity> {
}

