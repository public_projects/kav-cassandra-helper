package org.green.cassandra.repository.proximity_analysis;

import org.green.cassandra.entity.proximity_analysis.ProximityAnalysisInvestigationEntity;
import org.green.cassandra.entity.proximity_analysis.ProximityAnalysisObservationEntity;
import org.green.cassandra.repository.generic.GenericRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProximityAnalysisInvestigationRepository extends GenericRepository<ProximityAnalysisInvestigationEntity> {
}
