package org.green.cassandra.repository.proximity_analysis;

import org.green.cassandra.entity.proximity_analysis.ProximityAnalysisInvestigationStatusEntity;
import org.green.cassandra.repository.generic.GenericRepository;
import org.green.cassandra.repository.generic.GenericRepository9;
import org.springframework.stereotype.Repository;


@Repository
public interface ProximityAnalysisInvestigationStatusRepository extends GenericRepository9<ProximityAnalysisInvestigationStatusEntity> {
}
