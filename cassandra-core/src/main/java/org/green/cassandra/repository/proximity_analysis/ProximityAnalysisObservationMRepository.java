package org.green.cassandra.repository.proximity_analysis;

import org.green.cassandra.entity.proximity_analysis.ProximityAnalysisObservationIEntity;
import org.green.cassandra.entity.proximity_analysis.ProximityAnalysisObservationMEntity;
import org.green.cassandra.repository.generic.GenericRepository2;
import org.green.cassandra.repository.generic.GenericRepository3;
import org.springframework.stereotype.Repository;


@Repository
public interface ProximityAnalysisObservationMRepository extends GenericRepository3<ProximityAnalysisObservationMEntity> {
}
