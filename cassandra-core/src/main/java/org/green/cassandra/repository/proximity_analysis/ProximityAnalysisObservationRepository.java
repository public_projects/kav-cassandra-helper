package org.green.cassandra.repository.proximity_analysis;

import org.green.cassandra.entity.proximity_analysis.ProximityAnalysisObservationEntity;
import org.green.cassandra.repository.generic.GenericRepository;
import org.green.cassandra.repository.generic.GenericRepository2;
import org.springframework.stereotype.Repository;

@Repository
public interface ProximityAnalysisObservationRepository extends GenericRepository<ProximityAnalysisObservationEntity> {
}
