package org.green.cassandra;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.green.cassandra.entity.analysis.convoy.ConvoyEventEntity;
import org.green.cassandra.model.ConvoyEventModel;
import org.green.cassandra.repository.analysis.convoy.ConvoyEventRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SpringBootTest
class CassandraCoreApplicationTests {

	@Autowired
	private ConvoyEventRepository convoyEventRepository;

	@Test
	void getAllActivityByGeoHash() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		List<ConvoyEventEntity> entityList =
				convoyEventRepository.findByKey("f221cbe0-1a49-11ee-a406-2c0da7f630d4");

		Set<String> geoHashes = new HashSet<>();
		for (ConvoyEventEntity entity : entityList) {
			ConvoyEventModel convoyEventModel = objectMapper.readValue(entity.getValue(), ConvoyEventModel.class);
			geoHashes.addAll(convoyEventModel.getGeoHashes());
		}

		System.err.println("size: " + geoHashes.size());
		System.err.println(geoHashes);

		StringBuilder sb = new StringBuilder();
		for (String geoHash : geoHashes) {
			sb.append("\"").append(geoHash).append("\"").append(",");
		}
		System.err.println(sb);
	}
}
