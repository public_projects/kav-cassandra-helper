if not exist "release-package" mkdir release-package

COPY "target\cassandra-services-0.0.1-SNAPSHOT-exec.jar" "release-package\cassandra-services-0.0.1-SNAPSHOT-exec.jar"
COPY "target\classes\application.yml" "release-package\application.yml"

set SPRING_PROFILES_ACTIVE=LOCAL
cd release-package
CALL java -jar -Dserver.port=9010 cassandra-services-0.0.1-SNAPSHOT-exec.jar --spring.config.location=application.yml
pause
