package org.green.cassandra.services;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan({"org.green.cassandra",
				"org.green.kav.common.bean",
				"org.green.cassandra.astra.db",
				"org.green.cassandra.astra.db.config"})
public class CassandraServiceApplication {
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(CassandraServiceApplication.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
	}
}
