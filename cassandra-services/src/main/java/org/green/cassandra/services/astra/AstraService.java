package org.green.cassandra.services.astra;

import com.datastax.astra.sdk.AstraClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.datastax.oss.driver.api.core.cql.ResultSet;

@Lazy
@Service
public class AstraService {
    private final static Logger LOGGER = LoggerFactory.getLogger(AstraService.class);

    private AstraClient astraClient;

    @Lazy
    @Autowired
    public AstraService(AstraClient astraClient){
        this.astraClient = astraClient;
    }

    public void isConnected(){

        ResultSet rs = astraClient.cqlSession().execute("select * from \"Target_Identity_Count\"");
        rs.forEach(row -> {
            LOGGER.info("Data: {}", row.getString("name"));
        });
    }

    /**
     * INSERT INTO "Target_Identity_Count" (name, mcngid, identitytype, count)
     *         VALUES ('test', 123456, 'Marianne', 2)
     *
     * @param tableName
     */
    public void insert(String tableName, String[] columnNames, Object[] valueObjs, Class[] dataTypes) {
        String columns = "";
        for (String column : columnNames) {
            columns = columns + column + ", ";
        }

        if (valueObjs.length != dataTypes.length){
            throw new IllegalArgumentException("Data type count and value count does not match!");
        }

        String values = "";
        for (int i = 0; i < valueObjs.length; i++) {
            Class dataType = dataTypes[i];
            Object value = valueObjs[i];

            if (dataType == String.class){
                LOGGER.info("1. dataType: {}", dataType);
                values = values + "'" + value + "', ";
            } else if (dataType == Integer.class) {
                LOGGER.info("2. dataType: {}", dataType);
                values = values  + value + ", ";
            }
        }

        columns = columns.substring(0, columns.length()-2);
        values = values.substring(0, values.length()-2);

        String statement = "INSERT INTO \""+tableName+"\" ("+columns+") VALUES (" + values +")";

        LOGGER.info("statement: {}", statement);
    }
}
