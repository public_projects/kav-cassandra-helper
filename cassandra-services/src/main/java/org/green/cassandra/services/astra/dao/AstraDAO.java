package org.green.cassandra.services.astra.dao;

import java.util.List;

public interface AstraDAO<T> {
    boolean insert(T t);
    List<T> fetchAll();
    boolean remove(T t);
}
