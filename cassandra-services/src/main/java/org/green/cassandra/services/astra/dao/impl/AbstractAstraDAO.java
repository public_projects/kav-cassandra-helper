package org.green.cassandra.services.astra.dao.impl;

import com.datastax.astra.sdk.AstraClient;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import com.datastax.oss.driver.api.core.cql.Row;
import org.apache.pulsar.shade.org.apache.commons.lang.StringEscapeUtils;
import org.green.cassandra.services.astra.dao.AstraDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractAstraDAO<T> implements AstraDAO<T> {
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private AstraClient astraClient;

    protected AbstractAstraDAO(AstraClient astraClient) {
        this.astraClient = astraClient;
    }

    protected void insert(String tableName, String[] columnNames, Object[] valueObjs, Class[] dataTypes) {
        String columns = "";
        for (String column : columnNames) {
            columns = columns + column + ", ";
        }

        if (valueObjs.length != dataTypes.length) {
            throw new IllegalArgumentException("Data type count and value count does not match!");
        }

        String values = "";
        for (int i = 0; i < valueObjs.length; i++) {
            Class dataType = dataTypes[i];
            Object value = valueObjs[i];

            if (value == null) {
                LOGGER.info("Preparing {} for values: [{}] ", value, valueObjs);
                return;
            }
            
            if (dataType == String.class) {
                String escapeSql = StringEscapeUtils.escapeSql(value.toString());
                LOGGER.info("1. dataType: {}", dataType);
                values = values + "'" + escapeSql + "', ";
            }
            else if (dataType == Integer.class || dataType == Long.class) {
                LOGGER.info("2. dataType: {}", dataType);
                values = values + value + ", ";
            }
        }

        columns = columns.substring(0, columns.length() - 2);
        values = values.substring(0, values.length() - 2);

        String statement = "INSERT INTO \"" + tableName + "\" (" + columns + ") VALUES (" + values + ")";
        LOGGER.info("statement: {}", statement);
        ResultSet resultSet = astraClient.cqlSession().execute(statement);
        LOGGER.info("resultSet: {}", resultSet);
    }

    protected List<T> fetch(String tableName) {
        ResultSet rs = astraClient.cqlSession().execute("SELECT * FROM \"" + tableName + "\"");

        List<T> data = new ArrayList<>();
        rs.forEach(row -> {
            data.add(mapToEntity(row));
        });

        return data;
    }

    abstract T mapToEntity(Row row);

    boolean dropTable(String tableName) {
        astraClient.cqlSession().execute("DROP  table \"" + tableName + "\"");
        return true;
    }
}
