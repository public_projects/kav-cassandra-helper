package org.green.cassandra.services.astra.dao.impl;

import com.datastax.astra.sdk.AstraClient;

import com.datastax.oss.driver.api.core.cql.Row;
import org.green.cassandra.services.astra.entity.TargetIdentityCountEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import java.util.List;

@Lazy
@Service
public class TargetIdentityCountDAOImpl extends AbstractAstraDAO<TargetIdentityCountEntity> {

    private final String tableName = "Target_Identity_Count";
    private String[] columnNames = {"name", "mcngid", "identitytype", "count"};
    private Class[] dataTypes = {String.class, String.class, String.class, Long.class};

    @Lazy
    @Autowired
    public TargetIdentityCountDAOImpl(AstraClient astraClient) {
        super(astraClient);
    }

    @Override
    public boolean insert(TargetIdentityCountEntity targetIdentityCountEntity) {
        Object[] valueObjs = {targetIdentityCountEntity.getName(),
                              targetIdentityCountEntity.getMcngId(),
                              targetIdentityCountEntity.getIdentityType(),
                              targetIdentityCountEntity.getCount()};

        this.insert(tableName, columnNames, valueObjs, dataTypes);
        return true;
    }

    @Override
    public List<TargetIdentityCountEntity> fetchAll() {
        return fetch(tableName);
    }

    @Override
    TargetIdentityCountEntity mapToEntity(Row row) {
        TargetIdentityCountEntity entity = new TargetIdentityCountEntity(row.getString("name"),
                                                                         row.getString("mcngid"),
                                                                         row.getString("identitytype"),
                                                                         row.getInt("count"));

        LOGGER.info("Fetched: {}", entity);

        return entity;
    }

    @Override
    public boolean remove(TargetIdentityCountEntity targetIdentityCountEntity) {
        return false;
    }

}
