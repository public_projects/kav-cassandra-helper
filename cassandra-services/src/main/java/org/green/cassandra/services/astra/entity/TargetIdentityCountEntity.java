package org.green.cassandra.services.astra.entity;

import java.util.Objects;

public class TargetIdentityCountEntity {
    private String name;
    private String mcngId;
    private String identityType;
    private int count;

    public TargetIdentityCountEntity(String name, String mcngId, String identityType, int count){
        this.name = name;
        this.mcngId = mcngId;
        this.identityType = identityType;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMcngId() {
        return mcngId;
    }

    public void setMcngId(String mcngId) {
        this.mcngId = mcngId;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "TargetIdentityCountEntity{" + "name='" + name + '\'' + ", mcngId=" + mcngId + ", identityType='" + identityType + '\'' +
               ", count=" + count + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TargetIdentityCountEntity that = (TargetIdentityCountEntity) o;
        return mcngId == that.mcngId && Objects.equals(name, that.name) && Objects.equals(identityType, that.identityType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, mcngId, identityType);
    }
}
