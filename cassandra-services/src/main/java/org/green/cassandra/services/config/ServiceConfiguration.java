package org.green.cassandra.services.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfiguration {

    @Bean
    public ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }
//
//    @Bean
//    public
//    CqlSession session(@Value ("${spring.data.cassandra.keyspace-name}") String keyspace) {
//        return CqlSession.builder().withKeyspace(keyspace).build();
//    }
}
