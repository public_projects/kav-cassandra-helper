package org.green.cassandra.services.controller;

import org.green.cassandra.entity.AcCgiActivityEntity;
import org.green.cassandra.entity.TpTargetEntity;
import org.green.cassandra.services.core.CdrActivityService;
import org.green.cassandra.services.core.DeviceDetailService;
import org.green.cassandra.services.core.TargetService;
import org.green.cassandra.services.model.CdrShortModel;
import org.green.cassandra.services.model.request.CdrRequestModel;
import org.green.cassandra.services.model.request.IdentityModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
public class CassandraController {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private CdrActivityService cdrActivityService;

    @Autowired
    private TargetService targetService;

    @Autowired
    private DeviceDetailService deviceDetailService;

    @CrossOrigin
    @GetMapping("/greeting")
    public String getGreeting() {
        StopWatch watch = new StopWatch();
        watch.start();
        try {
            Thread.sleep(1000);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        watch.stop();
        log.info("hitter! {}", watch.getTotalTimeMillis());
        return "Hello man!";
    }

    @CrossOrigin
    @GetMapping("/device/activity/call")
    public String getDeviceCallActivity(@RequestBody IdentityModel identityModel) {
        StopWatch watch = new StopWatch();
        watch.start();
        try {
            Thread.sleep(1000);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        watch.stop();
        log.info("hitter! {}, msisdn: {}, imei: {}, :imsi: {}", watch.getTotalTimeMillis(), identityModel.getMsisd(), identityModel.getImei(), identityModel.getImsi());
        return "Hello man!";
    }

    @CrossOrigin
    @GetMapping("/msisdn/activities")
    public ResponseEntity<List<AcCgiActivityEntity>> getMsisdnActivity() {
        StopWatch watch = new StopWatch();
        watch.start();
        List<AcCgiActivityEntity> results = cdrActivityService.getAllCdrByCgi();
        watch.stop();
        log.info("results size {}, processed in {} ms.", results.size(), watch.getTotalTimeMillis());
        return new ResponseEntity(results, HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @GetMapping("/target/msisdn")
    public ResponseEntity<Map<String, String>> getAllTargetMsisdn() {
        StopWatch watch = new StopWatch();
        watch.start();
        Map<String, String> results = cdrActivityService.getAllTargetAndMsisdn();
        watch.stop();
        log.info("results size {}, processed in {} ms.", results.size(), watch.getTotalTimeMillis());
        return new ResponseEntity(results, HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @GetMapping("/target/msisdn/by/cgi")
    public ResponseEntity<List<CdrShortModel>> filterCdrWithMsisdnByCgi(@RequestBody CdrRequestModel cdrRequestModel) {
        StopWatch watch = new StopWatch();
        watch.start();
        List<CdrShortModel> results = cdrActivityService.filterCdrWithMsisdnByCgi(cdrRequestModel.getFromDateTimeStr(),
                                                                                  cdrRequestModel.getToDateTimeStr(),
                                                                                  cdrRequestModel.getCgiA(),
                                                                                  cdrRequestModel.getCgiB(),
                                                                                  cdrRequestModel.getMsisdnA(),
                                                                                  cdrRequestModel.getMsisdnB(),
                                                                                  cdrRequestModel.isWriteToFileOriginal(),
                                                                                  cdrRequestModel.isWriteToFileFiltered());
        watch.stop();
        log.info("results size {}, processed in {} ms.", results.size(), watch.getTotalTimeMillis());
        return new ResponseEntity(results, HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @GetMapping("/target/by/name")
    public ResponseEntity<TpTargetEntity> getTargetByName(@RequestBody Map<String, String>  requestBody) {
        StopWatch watch = new StopWatch();
        watch.start();
        TpTargetEntity targetEntity = this.targetService.getTargetByName(requestBody.get("name"));
        watch.stop();
        log.info("results size {}, processed in {} ms.", targetEntity, watch.getTotalTimeMillis());
        return ResponseEntity.ok(targetEntity);
    }

    @CrossOrigin
    @GetMapping("/all/device/detail")
    public ResponseEntity<String> getAllDeviceDetail() {
        StopWatch watch = new StopWatch();
        watch.start();
        this.deviceDetailService.getAllDeviceDetail();
        watch.stop();
        return ResponseEntity.ok("All good");
    }
}
