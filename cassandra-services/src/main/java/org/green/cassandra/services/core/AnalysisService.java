package org.green.cassandra.services.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.green.cassandra.entity.analysis.InvestigationManagementEntity;
import org.green.cassandra.entity.analysis.convoy.ObservationResultEntity;
import org.green.cassandra.entity.analysis.convoy.ObservationSubjectEntity;
import org.green.cassandra.entity.investigation.InvestigationSearchEntity;
import org.green.cassandra.services.core.model.ImportProcessMetadata;
import org.green.cassandra.services.core.model.convoy.ConvoyAnalysisSubject;
import org.green.cassandra.services.core.model.InvestigationManagement;
import org.green.cassandra.services.core.model.convoy.ConvoyObservationEvent;

import java.util.List;
import java.util.Set;

public interface AnalysisService {
    List<InvestigationManagementEntity> getAllInvestigationManagement();
    List<InvestigationSearchEntity> getAllInvestigations();
    List<InvestigationSearchEntity> getAllInvestigationByType(String type);
    InvestigationSearchEntity  getInvestigationByName(String investigationName);
    InvestigationManagement getInvestigationManagementByKey(String investigationId);
    ConvoyObservationEvent getObservationEvent(String investigationId, String observationId);
    ObservationSubjectEntity getObservationSubjectByKey(String key);
    ConvoyAnalysisSubject getSubject(String key);
    List<ConvoyAnalysisSubject> getAllConvoySubject() throws Exception;
    ObservationResultEntity getObservationResult(String key);
    String getInvestigationStatus(String key) throws Exception;
    String getInvestigationName(String key) throws Exception;
    List<ConvoyObservationEvent> getAllEventByInvestigationId(String investigationId);
    List<ConvoyObservationEvent> getAllEventByInvestigationName(String investigationName);
    List<ConvoyObservationEvent> getAllEventByAllInvestigation();

    InvestigationManagement getInvestigationByKey(String key) throws JsonProcessingException;

    Set<String> getProcessId(String id) throws Exception;

    List<InvestigationSearchEntity> getProcessIdByConvoyInvestigationId(String investigationId);

    ImportProcessMetadata getImportProcessMetadata(String processId);
}
