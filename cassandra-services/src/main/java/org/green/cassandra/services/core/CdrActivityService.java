package org.green.cassandra.services.core;

import org.green.cassandra.entity.AcCgiActivityEntity;
import org.green.cassandra.services.model.CdrShortModel;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;


public interface CdrActivityService {

    List<AcCgiActivityEntity> getAllCdrByCgi();

    List<CdrShortModel> filterCdrByCgi(String fromDateTimeStr, String toDateTimeStr, List<String> cgis, boolean writeToFileFiltered);

    /**
     * @param fromDateTimeStr in yyyy-MM-dd HH:mm:ss format
     * @param toDateTimeStr   in yyyy-MM-dd HH:mm:ss format
     * @param cgiA
     * @param cgiB
     * @param msisdnA
     * @param msisdnB
     * @param writeToFileOriginal
     * @param writeToFileFiltered
     * @return
     */
    List<CdrShortModel> filterCdrWithMsisdnByCgi(String fromDateTimeStr,
                                                 String toDateTimeStr,
                                                 String cgiA,
                                                 String cgiB,
                                                 String msisdnA,
                                                 String msisdnB,
                                                 boolean writeToFileOriginal,
                                                 boolean writeToFileFiltered);

    List<CdrShortModel> filterCdrByMsisdn(String fromDateTimeStr,
                                                 String toDateTimeStr,
                                                 String msisdnA,
                                                 boolean writeToFileFiltered);

    List<CdrShortModel> filterCdrByMsisdn(String fromDateTimeStr,
                                          String toDateTimeStr,
                                          List<String> msisdns,
                                          boolean writeToFileFiltered);

    Map<String, String> getAllTargetAndMsisdn();
}
