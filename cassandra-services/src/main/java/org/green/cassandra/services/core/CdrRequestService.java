package org.green.cassandra.services.core;

import org.green.cassandra.services.core.model.dataretention.AcCdrRequestStatusModel;

import java.util.List;

public interface CdrRequestService {
    List<AcCdrRequestStatusModel> getAllCdrRequestStatuses();
}
