package org.green.cassandra.services.core;

import org.green.cassandra.entity.ImeiTypeAllocationCodeEntity;

import java.util.List;

public interface DeviceDetailService {
    List<ImeiTypeAllocationCodeEntity> getAllDeviceDetail();
    boolean writeToDB(String inputFile);
}
