package org.green.cassandra.services.core;

import java.util.List;

public interface McngIdService {
    List<Long> getTargetMcngId();
    List<Long> getAllMcngId();
}
