package org.green.cassandra.services.core;

import org.green.cassandra.entity.TpTargetEntity;


public interface TargetService {

    long getTargetCount();

    TpTargetEntity getTargetById(String uuid);

    TpTargetEntity saveTarget(TpTargetEntity tpTargetEntity);

    TpTargetEntity removeTarget(String uuid);

    TpTargetEntity getTargetByName(String targetName);
}
