package org.green.cassandra.services.core;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.green.cassandra.entity.AcMsisdnActivityEntity;
import org.green.cassandra.services.model.CdrShortModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.WeekFields;
import java.util.*;
import java.util.stream.Collectors;

public class Utility {
    private static final Logger log = LoggerFactory.getLogger(Utility.class);
    public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static ObjectMapper mapper = new ObjectMapper();

    /**
     * @param startDateStr expected in yyyy-MM-dd HH:mm:ss, e.g: 2020-10-31 00:00:00
     * @param endDateStr
     * @return
     */
    public static List<Long> getWeeksBetween2Dates(String startDateStr, String endDateStr) {
        LocalDateTime startDateTime = LocalDateTime.parse(startDateStr, formatter);
        LocalDateTime endDateTime = LocalDateTime.parse(endDateStr, formatter);

        long dateDiff = ChronoUnit.DAYS.between(startDateTime, endDateTime);

        Set<Long> weekOfTheYears = new HashSet<>();
        LocalDateTime newDateTime;
        for (int i = 0; i <= dateDiff; i++) {
            newDateTime = startDateTime.plus(i, ChronoUnit.DAYS);

            OffsetDateTime newOffsetDateTime = OffsetDateTime.ofInstant(newDateTime.toInstant(ZoneOffset.UTC), ZoneOffset.UTC);
            int year = newOffsetDateTime.get(WeekFields.ISO.weekBasedYear());
            int week = newOffsetDateTime.get(WeekFields.ISO.weekOfWeekBasedYear());
            log.info("1. {} is {}", newDateTime.format(formatter), year+""+week);

            LocalDate date = LocalDate.of(newDateTime.getYear(), newDateTime.getMonth(), newDateTime.getDayOfMonth());
            int weekOfYear = date.get(WeekFields.of(Locale.getDefault()).weekOfYear());

            String weekYear = newDateTime.getYear() + "" + weekOfYear;
            log.info("2. {} is {}", newDateTime.format(formatter), weekYear);
            weekOfTheYears.add(Long.parseLong(weekYear));
        }

        List<Long> weekYears = weekOfTheYears.stream().sorted().collect(Collectors.toList());

        // Add 1 week before and 1 week after the last value
        if (!weekYears.isEmpty()) {
            Long firstValue = weekYears.get(0);
            Long lastValue = weekYears.get(weekYears.size() - 1);
            weekYears.add(firstValue - 1);
            weekYears.add(lastValue + 1);
        }

        return weekYears;
    }

    public static List<CdrShortModel> cdrShortModelMapper(List<String> values) {
        List<CdrShortModel> cdrShortModels = new ArrayList<>();
        log.info("start,msisdnA,msisdnB,imeaA,imeaB,imsiA,imsiB,cgiA,cgiB,callType,smsContent,duration,direction");
        for (int i = 0; i < values.size(); i++) {
            cdrShortModels.add(strCdrToModel(values.get(i)));
        }
        log.info("End");

        return cdrShortModels;
    }

    public static CdrShortModel strCdrToModel(String strValue) {
        CdrShortModel cdrShortModel;
        cdrShortModel = new CdrShortModel();
        try {
            JsonNode jsonNode = mapper.readTree(strValue);
            String start = jsonNode.get("start")
                                   .asText();
            String msisdnA = jsonNode.get("msisdnA")
                                     .asText();
            String msisdnB = jsonNode.get("msisdnB")
                                     .asText();
            String imeiA = getNodeStringValue(jsonNode, "imeiA");
            String imeiB = getNodeStringValue(jsonNode, "imeiB");
            String imsiA = getNodeStringValue(jsonNode, "imsiA");
            String imsiB = getNodeStringValue(jsonNode, "imsiB");
            String cgiA = constructCgi(jsonNode, "cellsA");
            String cgiB = constructCgi(jsonNode, "cellsB");
            String callType = getNodeStringValue(jsonNode, "callType");
            String smsContent = getNodeStringValue(jsonNode, "smsContent");
            String duration = getNodeStringValue(jsonNode, "duration");
            int direction = jsonNode.get("direction")
                                    .asInt();

            cdrShortModel.setStart(new Long(start));
            cdrShortModel.setMsisdnA(msisdnA);
            cdrShortModel.setMsisdnB(msisdnB);
            cdrShortModel.setImeaA(imeiA);
            cdrShortModel.setImeaB(imeiB);
            cdrShortModel.setImsiA(imsiA);
            cdrShortModel.setImsiB(imsiB);
            cdrShortModel.setCgiA(cgiA);
            cdrShortModel.setCgiB(cgiB);
            cdrShortModel.setCallType(callType);
            cdrShortModel.setSmsContent(smsContent);
            cdrShortModel.setDuration(duration);
            cdrShortModel.setDirection(direction);

            Instant instant = Instant.ofEpochMilli(Long.parseLong(start));
            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
            cdrShortModel.setStartLocalDateTime(localDateTime);
            cdrShortModel.setStart(Long.parseLong(start));
            cdrShortModel.setStartShort(Utility.formatter.format(localDateTime));

            log.info("{},{},{},{},{},{},{},{},{},{},{},{},{},{}",
                     localDateTime.format(Utility.formatter),
                     msisdnA,
                     msisdnB,
                     imeiA,
                     imeiB,
                     imsiA,
                     imsiB,
                     cgiA,
                     cgiB,
                     callType,
                     smsContent,
                     duration,
                     direction);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        return cdrShortModel;
    }

    private static String constructCgi(String cellA) throws Exception {
        if (!cellA.isEmpty()) {
            Object[] cellBObj = mapper.readValue(cellA, Object[].class);
            if (cellBObj.length > 0) {
                Map<String, Object> cellBMap = (Map<String, Object>) cellBObj[0];
                Map<String, Object> cgiMap = (Map<String, Object>) cellBMap.get("cellGlobalId");
                return cgiMap.get("mcc")
                             .toString() + "-" + cgiMap.get("mnc")
                                                       .toString() + "-" + cgiMap.get("lac")
                                                                                 .toString() + "-" + cgiMap.get("cellId")
                                                                                                           .toString();
            }
        }
        return "";
    }

    public static List<CdrShortModel> cdrShortModelMapper(List<AcMsisdnActivityEntity> acMsisdnActivityEntities,
                                                          boolean toWriteToFile,
                                                          String fileNamePostfix) {
        List<String> values = new ArrayList<>();

        for (AcMsisdnActivityEntity acMsisdnActivityEntity : acMsisdnActivityEntities) {
            values.add(acMsisdnActivityEntity.getValue());
        }

        List<CdrShortModel> cdrShortModels = cdrShortModelMapper(values);
        if (toWriteToFile) Utility.cdrCsvFile(cdrShortModels, fileNamePostfix);
        return cdrShortModels;
    }

    private static String constructCgi(JsonNode jsonNode, String keyName) throws Exception {
        String cell = jsonNode.get(keyName)
                .toString();

        return constructCgi(cell);
    }

    private static String getNodeStringValue(JsonNode jsonNode, String keyName) {
        JsonNode jsonNode1 = jsonNode.get(keyName);

        if (jsonNode1 == null) {
            return "";
        }

        return jsonNode1.asText();
    }


    public static void cdrCsvFile(List<CdrShortModel> cdrShortModels, String fileNamePost) {
        StringBuilder sb = new StringBuilder();
        sb.append("start,msisdnA,msisdnB,imeaA,imeaB,imsiA,imsiB,cgiA,cgiB,callType,smsContent,duration,direction")
          .append(System.lineSeparator());

        for (CdrShortModel cdrShortModel : cdrShortModels) {
            if (cdrShortModel.getDirection() == 2) {

                sb.append(cdrShortModel.getStartShort())
                  .append(",")
                  .append(cdrShortModel.getMsisdnA())
                  .append(",")
                  .append(cdrShortModel.getMsisdnB())
                  .append(",")
                  .append(cdrShortModel.getImeaA())
                  .append(",")
                  .append(cdrShortModel.getImeaB())
                  .append(",")
                  .append(cdrShortModel.getImsiA())
                  .append(",")
                  .append(cdrShortModel.getImsiB())
                  .append(",")
                  .append(cdrShortModel.getCgiA())
                  .append(",")
                  .append(cdrShortModel.getCgiB())
                  .append(",")
                  .append(cdrShortModel.getCallType())
                  .append(",")
                  .append(cdrShortModel.getSmsContent())
                  .append(",")
                  .append(cdrShortModel.getDuration())
                  .append(",")
                  .append(cdrShortModel.getDirection())
                  .append(System.lineSeparator());
            }
        }

        writeToFile(sb.toString(),
                    "msisdn_" + fileNamePost + "_" + Instant.now()
                                                            .toEpochMilli() + ".csv");
    }

    public static void writeToFile(String jsonStr, String filePath) {
        File jsonFile = new File(filePath);
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(jsonFile));
            writer.write(jsonStr);
        }
        catch (IOException ioe) {
            log.error("Failed to write to file: {}", ioe);
        }
        finally {
            try {
                writer.close();
            }
            catch (IOException io) {
                // Just ignore
            }
        }
        log.info("Written to {}", jsonFile.getAbsolutePath());
    }
}
