package org.green.cassandra.services.core.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.green.cassandra.entity.analysis.ImportProcessMetadataEntity;
import org.green.cassandra.entity.analysis.InvestigationManagementEntity;
import org.green.cassandra.entity.analysis.convoy.ObservationEventEntity;
import org.green.cassandra.entity.analysis.convoy.ObservationResultEntity;
import org.green.cassandra.entity.analysis.convoy.ObservationSubjectEntity;
import org.green.cassandra.entity.investigation.InvestigationSearchEntity;
import org.green.cassandra.entity.proximity_analysis.model.PaInvestigationModel;
import org.green.cassandra.repository.analysis.ImportProcessMetadataRepository;
import org.green.cassandra.repository.analysis.InvestigationManagementRepository;
import org.green.cassandra.repository.analysis.convoy.ObservationEventRepository;
import org.green.cassandra.repository.analysis.convoy.ObservationResultRepository;
import org.green.cassandra.repository.analysis.convoy.ObservationSubjectRepository;
import org.green.cassandra.repository.investigation.InvestigationSearchRepository;
import org.green.cassandra.services.core.AnalysisService;
import org.green.cassandra.services.core.model.ImportProcessMetadata;
import org.green.cassandra.services.core.model.convoy.ConvoyAnalysisSubject;
import org.green.cassandra.services.core.model.InvestigationManagement;
import org.green.cassandra.services.core.model.convoy.ConvoyAnalysisSubjectIdentity;
import org.green.cassandra.services.core.model.convoy.ConvoyAnalysisSubjectTarget;
import org.green.cassandra.services.core.model.convoy.ConvoyObservationEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class AnalysisServiceImpl implements AnalysisService {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private InvestigationSearchRepository investigationSearchRepository;

    @Autowired
    private ObservationSubjectRepository observationSubjectRepository;

    @Autowired
    private ObservationResultRepository observationResultRepository;

    @Autowired
    private InvestigationManagementRepository investigationManagementRepository;

    @Autowired
    private ObservationEventRepository observationEventRepository;

    @Autowired
    private ImportProcessMetadataRepository importProcessMetadataRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public List<InvestigationSearchEntity> getAllInvestigationByType(String keyType) {
        return investigationSearchRepository.findByKey(keyType);
    }

    @Override
    public Set<String> getProcessId(String id) throws Exception {
        List<InvestigationManagementEntity> entities = investigationManagementRepository.findAll();
        Set<String> processIds = new HashSet<>();
        StringBuilder sb = new StringBuilder();
        sb.append(InvestigationManagementEntity.getHeader());
        for (InvestigationManagementEntity managementEntity : entities) {
            PaInvestigationModel paInvestigationModel = objectMapper.readValue(managementEntity.getValue(), PaInvestigationModel.class);
            if (paInvestigationModel.getInvestigationName().equalsIgnoreCase(id)) {
                sb.append(managementEntity.toString());
                String processId = managementEntity.getKey();
                System.err.println("[InvestigationManagement_v2](key)Process id: " + processId + ", for given id: "+ id);
                processIds.add(processId);
            }
        }
        return processIds;
    }

    @Override
    public List<InvestigationManagementEntity> getAllInvestigationManagement() {
        return investigationManagementRepository.findAll();
    }

    @Override
    public List<InvestigationSearchEntity> getAllInvestigations() {
        return this.investigationSearchRepository.findAll();
    }

    @Override
    public InvestigationSearchEntity getInvestigationByName(String investigationName) {
        Optional<InvestigationSearchEntity> optionalValue = this.investigationSearchRepository.findAll()
                                                                              .stream()
                                                                              .filter(investigationSearchEntity -> investigationSearchEntity.getValue().equalsIgnoreCase(investigationName))
                                                                              .findAny();

        return optionalValue.isPresent() == true ? optionalValue.get() : null;
    }

    @Override
    public InvestigationManagement getInvestigationManagementByKey(String investigationId) {
        InvestigationManagementEntity investigationManagementEntity = investigationManagementRepository.findFirstByKeyOrderByColumn1Desc(
                investigationId);

        InvestigationManagement investigationManagement = null;
        if (investigationManagementEntity == null) return null;
        try {
            investigationManagement = objectMapper.readValue(investigationManagementEntity.getValue(), InvestigationManagement.class);
            JsonNode caNode = objectMapper.readTree(investigationManagementEntity.getValue());
            JsonNode observationListNode = caNode.get("observationList");

            if (observationListNode != null) {
                String observationStr = observationListNode.textValue();
                if (observationStr != null && !observationStr.isEmpty()) {
                    investigationManagement.setObservationList(Arrays.asList(observationStr.split("#")));
                }
            }
        } catch (JsonProcessingException e1) {
            try {
                JsonNode caNode = objectMapper.readTree(investigationManagementEntity.getValue());
                JsonNode createdDateNode = caNode.get("creationDate");
                JsonNode investigationNameNode = caNode.get("investigationName");
                JsonNode investigationTypeNode = caNode.get("investigationType");
                JsonNode investigationStatusNode = caNode.get("investigationStatus");
                JsonNode observationListNode = caNode.get("observationList");

                investigationManagement = new InvestigationManagement();
                investigationManagement.setCreationDate(createdDateNode.asLong());
                investigationManagement.setInvestigationType(investigationTypeNode.asText());
                investigationManagement.setInvestigationName(investigationNameNode.asText());
                if (investigationStatusNode != null)
                    investigationManagement.setInvestigationStatus(investigationStatusNode.asText());

                if (observationListNode != null) {
                    String observationStr = observationListNode.textValue();
                    if (observationStr != null && !observationStr.isEmpty()) {
                        investigationManagement.setObservationList(Arrays.asList(observationStr.split("#")));
                    }
                }
            } catch (JsonProcessingException e2) {
                e2.printStackTrace();
                return null;
            }
        }

        investigationManagement.setKey(investigationManagementEntity.getKey());
        return investigationManagement;
    }

    @Override
    public ConvoyObservationEvent getObservationEvent(String investigationId, String observationId) {
        List<ObservationEventEntity> observationEventEntities = observationEventRepository.findByKeyAndKey2(investigationId, observationId);
        ConvoyObservationEvent convoyObservationEvent = new ConvoyObservationEvent();
        if (observationEventEntities != null) {
            observationEventEntities.forEach(observationEventEntity -> {
                String column = observationEventEntity.getColumn1();
                String value = observationEventEntity.getValue();
                if (column.equalsIgnoreCase("CallTime")) {
                    convoyObservationEvent.setCallTime(Long.parseLong(value));
                } else if (column.equalsIgnoreCase("Position")){
                    convoyObservationEvent.setPosition(value);
                } else if (column.equalsIgnoreCase("Source")) {
                    convoyObservationEvent.setSource(value);
                }
            });
        }
        return convoyObservationEvent;
    }

    @Override
    public ObservationSubjectEntity getObservationSubjectByKey(String key) {
        List<ObservationSubjectEntity> observationSubjectEntities = observationSubjectRepository.findByKey(key);
        return observationSubjectEntities != null && !observationSubjectEntities.isEmpty() ? observationSubjectEntities.get(0) : null;
    }

    @Override
    public ConvoyAnalysisSubject getSubject(String key) {
        ObservationSubjectEntity observationSubjectEntity = getObservationSubjectByKey(key);

        ConvoyAnalysisSubject convoyAnalysisSubject = null;
        if (observationSubjectEntity == null) {
            return null;
        }
        else {
            try {
                convoyAnalysisSubject = objectMapper.readValue(observationSubjectEntity.getValue(), ConvoyAnalysisSubject.class);
            } catch (JsonProcessingException e1) {
                try {
                    JsonNode caNode = objectMapper.readTree(observationSubjectEntity.getValue());
                    JsonNode createdDate = caNode.get("creationDate");
                    ArrayNode observedTargetNodes = (ArrayNode) caNode.get("observedTargets");

                    for (JsonNode targetNode: observedTargetNodes) {
                        String targetId = targetNode.get("targetId").asText();
                        String targetName = targetNode.get("name").asText();
                        ArrayNode targetIdentities = (ArrayNode) targetNode.get("targetIdentities");
                        List<ConvoyAnalysisSubjectIdentity> subjectIdentities = new ArrayList<>();

                        for (JsonNode targetIdentityNode : targetIdentities) {
                            ConvoyAnalysisSubjectIdentity subjectIdentity = new ConvoyAnalysisSubjectIdentity();
                            JsonNode identityTypeNode = targetIdentityNode.get("type");
                            subjectIdentity.setIdentityId(targetIdentityNode.get("id").asText());
                            subjectIdentity.setIdentityNature(identityTypeNode.get("name").asText());
                            subjectIdentities.add(subjectIdentity);
                        }

                        ConvoyAnalysisSubjectTarget targetSubject = new ConvoyAnalysisSubjectTarget(targetId, targetName, subjectIdentities);
                        targetSubject = objectMapper.readValue(targetNode.toString(), ConvoyAnalysisSubjectTarget.class);
                    }

                    ArrayNode identityNodes = (ArrayNode) caNode.get("identities");

                    convoyAnalysisSubject = new ConvoyAnalysisSubject();
                    convoyAnalysisSubject.setCreationDate(createdDate.asLong());
                } catch (JsonProcessingException e2) {
                    e2.printStackTrace();
                    return null;
                }
            }
        }

        return convoyAnalysisSubject;
    }

    public static void traverse(StringBuilder sb, JsonNode root) {
        if (root == null) {
            return;
        }

        sb.append(root.toPrettyString());

        if (root.isObject()) {
            Iterator<String> fieldNames = root.fieldNames();

            while (fieldNames.hasNext()) {
                String fieldName = fieldNames.next();
                JsonNode fieldValue = root.get(fieldName);
                traverse(sb, fieldValue);
            }
        }
        else if (root.isArray()) {
            ArrayNode arrayNode = (ArrayNode) root;
            for (int i = 0; i < arrayNode.size(); i++) {
                JsonNode arrayElement = arrayNode.get(i);
                traverse(sb, arrayElement);
            }
        }
        else {
            // JsonNode root represents a single value field - do something with it.
        }
    }

    @Override
    public List<ConvoyAnalysisSubject> getAllConvoySubject() throws Exception {
        StringBuilder sb = new StringBuilder();

       List<ObservationSubjectEntity> entities = observationSubjectRepository.findAll();

         for (int i = 0; i < entities.size(); i++) {
             ObservationSubjectEntity entity = entities.get(i);
             String key = entity.getKey();

             InvestigationManagement investigationManagement = getInvestigationByKey(key);

             sb.append(System.lineSeparator())
               .append(i+1)
               .append(") ")
               .append("investigation id: ").append(key).append(", investigationName: ").append(
                     investigationManagement != null ? investigationManagement.getInvestigationName() : null).append(System.lineSeparator());

             this.getObservationSubject(sb,
                                        entity.getKey(),
                                        entity.getValue());
        }

        log.info(sb.toString());
        return null;
    }

    private void getObservationSubject(StringBuilder sb, String key, String value) {
        try {
            JsonNode subjectNode = objectMapper.readTree(value);
            JsonNode identitiesNode = subjectNode.get("identities");
            ArrayNode observedTargets = (ArrayNode) subjectNode.get("observedTargets");
            if (identitiesNode != null && !identitiesNode.isEmpty()) {
                sb.append("[identities]")
                  .append(System.lineSeparator())
                  .append(identitiesNode.toPrettyString())
                  .append(System.lineSeparator());
            }

            if (observedTargets != null && !observedTargets.isEmpty()) {
                for (JsonNode observedTarget : observedTargets) {
                    JsonNode nameNode = observedTarget.get("name");
                    sb.append("[targetName]").append(nameNode.toPrettyString()).append(System.lineSeparator());
                    ArrayNode targetIdentitiesNodes = (ArrayNode) observedTarget.get("targetIdentities");
                    if (targetIdentitiesNodes != null && !targetIdentitiesNodes.isEmpty()) {
                        sb.append("[targetIdentities]").append(System.lineSeparator()).append(targetIdentitiesNodes.toPrettyString()).append(
                                System.lineSeparator()).append(System.lineSeparator());
                    }
                }
            }
        } catch (JsonProcessingException e) {
            log.info("{}investigation: {}, value: {}", System.lineSeparator(), key, value);
           e.printStackTrace();
        }
    }

    @Override
    public ObservationResultEntity getObservationResult(String key) {
        List<ObservationResultEntity> observationSubjectEntities = observationResultRepository.findByKey(key);
        ObservationResultEntity observationResultEntity = null;

        if (observationSubjectEntities == null || observationSubjectEntities.isEmpty()) {
            return null;
        }
        else {
            observationResultEntity = observationSubjectEntities.get(0);
        }

        return observationResultEntity;
    }

    @Override
    public String getInvestigationStatus(String key) throws Exception {
        InvestigationManagement investigationManagement = getInvestigationByKey(key);
        return investigationManagement != null ? investigationManagement.getInvestigationStatus() : null;
    }

    @Override
    public String getInvestigationName(String key) throws Exception {
        InvestigationManagement investigationManagement = getInvestigationByKey(key);
        return investigationManagement != null ? investigationManagement.getInvestigationName() : null;
    }

    @Override
    public List<ConvoyObservationEvent> getAllEventByInvestigationId(String investigationId) {
        InvestigationManagement investigationManagement = getInvestigationManagementByKey(investigationId);

        List<ConvoyObservationEvent> convoyObservationEvents = new ArrayList<>();
        List<String> observations = investigationManagement.getObservationList();

        if (observations != null) {
            observations.forEach(s -> {
                ConvoyObservationEvent convoyObservationEvent = getObservationEvent(investigationId, s);
                convoyObservationEvents.add(convoyObservationEvent);
            });
        }

        return convoyObservationEvents;
    }

    @Override
    public List<ConvoyObservationEvent> getAllEventByInvestigationName(String investigationName) {
        InvestigationSearchEntity investigationSearchEntity = getInvestigationByName(investigationName);
        if (investigationSearchEntity!=null) return getAllEventByInvestigationId(investigationSearchEntity.getColumn2());
        return null;
    }

    @Override
    public List<ConvoyObservationEvent> getAllEventByAllInvestigation() {
        List<ConvoyObservationEvent> events = new ArrayList<>();
        List<InvestigationSearchEntity> entities = getAllInvestigationByType("CONVOY_ANALYSIS");

        for (InvestigationSearchEntity entity: entities) {
            String investigationId = entity.getColumn2();
            String investigationName = entity.getValue();
            log.info("id: {}, name: {}", investigationId, investigationName);

            List<ConvoyObservationEvent> fetchedEvents = getAllEventByInvestigationId(investigationId);

            if (fetchedEvents != null) {
                fetchedEvents.forEach(convoyObservationEvent -> {
                    System.err.println("name: "+investigationName+"; "+ convoyObservationEvent.getSource());
                    events.add(convoyObservationEvent);
                });
            }
        }
        return events;
    }

    @Override
    public InvestigationManagement getInvestigationByKey(String key) throws JsonProcessingException {
        List<InvestigationManagementEntity> investigationManagementEntities = investigationManagementRepository.findByKey(key);

        Collections.sort(investigationManagementEntities);

        InvestigationManagement investigationManagement = new InvestigationManagement();
        if (investigationManagementEntities == null || investigationManagementEntities.isEmpty()) return null;
        InvestigationManagementEntity entity = investigationManagementEntities.get(0);
        String value = entity.getValue();

        JsonNode node = objectMapper.readTree(value);
        JsonNode investigationNameNode = node.get("investigationName");
        JsonNode investigationTypeNode = node.get("investigationType");
        JsonNode investigationStatusNode = node.get("investigationStatus");
        JsonNode observedNode =  node.get("observationList");

        if (observedNode != null){
            String[] observed = observedNode.asText().split("#");
            investigationManagement.setObservationList(Arrays.asList(observed));
        }

        investigationManagement.setInvestigationName(investigationNameNode.asText());
        investigationManagement.setInvestigationType(investigationTypeNode.asText());
        investigationManagement.setInvestigationStatus(investigationStatusNode.asText());
        investigationManagement.setKey(key);
        investigationManagement.setColumn1(entity.getColumn1());
        return investigationManagement;
    }

    public List<InvestigationSearchEntity> getProcessIdByConvoyInvestigationId(String investigationId) {
        List<InvestigationSearchEntity> entities = getAllInvestigations();

        StringBuilder sb = new StringBuilder("key;column1;column2;value\n");
        List<InvestigationSearchEntity> investigationSearchEntities = new ArrayList<>();
        for (InvestigationSearchEntity entity : entities) {
            if (entity.getValue().equalsIgnoreCase(investigationId)) { // This only will work for convoy analysis
                sb.append(entity.getKey()).append(";");
                sb.append(entity.getColumn1()).append(";");
                sb.append(entity.getColumn2()).append(";");
                sb.append(entity.getValue()).append(System.lineSeparator());
                investigationSearchEntities.add(entity);
            }
        }
//        System.err.println(sb);
//        System.err.println();
        return investigationSearchEntities;
    }

    @Override
    public ImportProcessMetadata getImportProcessMetadata(String processId) {

        ImportProcessMetadata importProcessMetadata = new ImportProcessMetadata(processId);
        List<ImportProcessMetadataEntity> entities = importProcessMetadataRepository.findByKey(processId);

        for (ImportProcessMetadataEntity metadataEntity: entities){
            importProcessMetadata.setValue(metadataEntity);
        }

        return importProcessMetadata;
    }
}
