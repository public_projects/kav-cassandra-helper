package org.green.cassandra.services.core.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import org.green.cassandra.entity.AcCgiActivityEntity;
import org.green.cassandra.entity.AcMsisdnActivityEntity;
import org.green.cassandra.entity.TpTargetMsisdnEntity;
import org.green.cassandra.repository.AcCgiActivityRepository;
import org.green.cassandra.repository.AcMsisdnActivityRepository;
import org.green.cassandra.repository.TpTargetMsisdnRepository;
import org.green.cassandra.repository.TpTargetRepository;
import org.green.cassandra.services.core.CdrActivityService;
import org.green.cassandra.services.core.Utility;
import org.green.cassandra.services.model.CdrShortModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class CdrActivityServiceImpl implements CdrActivityService {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private AcMsisdnActivityRepository acMsisdnActivityRepository;

    @Autowired
    private AcCgiActivityRepository acCgiActivityRepository;

    @Autowired
    private TpTargetMsisdnRepository tpTargetMsisdnRepository;

    @Autowired
    private TpTargetRepository tpTargetRepository;

    public CdrActivityServiceImpl() {
    }

    public List<AcCgiActivityEntity> getAllCdrByCgi() {
        return acCgiActivityRepository.findAll();
    }

    public List<Map<String, String>> getAllCdrByMsisdn() {
        List<Map<String, String>> msisdnActiviteContainer = new ArrayList<>();

        List<AcCgiActivityEntity> acCgiActivityEntities = acCgiActivityRepository.findAll();
        acCgiActivityEntities.forEach(acCgiActivityEntity -> {
            if (acCgiActivityEntity.getKey()
                                   .equals("")) {
                log.info("MSISDN: {}, date: {}", acCgiActivityEntity.getKey(), acCgiActivityEntity.getKey2());
                Map<String, String> msisdnActivityMap = new HashMap<>();
                msisdnActivityMap.put("MSISDN", acCgiActivityEntity.getKey());
                msisdnActivityMap.put("activity", acCgiActivityEntity.getKey2());
                msisdnActiviteContainer.add(msisdnActivityMap);
            }
        });

        return msisdnActiviteContainer;
    }

    public List<CdrShortModel> filterCdrByMsisdn(String fromDateTimeStr, String toDateTimeStr, String msisdn, boolean writeToFileFiltered) {
        List<Long> yearWeeks = Utility.getWeeksBetween2Dates(fromDateTimeStr, toDateTimeStr);
        return filterCdrByMsisdn(yearWeeks, msisdn, writeToFileFiltered);
    }

    public List<CdrShortModel> filterCdrByMsisdn(List<Long> yearWeeks, String msisdn, boolean writeToFileFiltered) {
        List<AcMsisdnActivityEntity> acMsisdnActivityEntities = acMsisdnActivityRepository.findAllByKeyAndAndKey2In(msisdn, yearWeeks);
        Utility.mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        return Utility.cdrShortModelMapper(acMsisdnActivityEntities, writeToFileFiltered, "cdr-by-msisdn-" + msisdn);
    }

    @Override
    public List<CdrShortModel> filterCdrByMsisdn(String fromDateTimeStr,
                                                 String toDateTimeStr,
                                                 List<String> msisdns,
                                                 boolean writeToFileFiltered) {
        List<Long> yearWeeks = Utility.getWeeksBetween2Dates(fromDateTimeStr, toDateTimeStr);
        List<CdrShortModel> allData = new ArrayList<>();
        msisdns.forEach(s -> {
            List<AcMsisdnActivityEntity> acMsisdnActivityEntities = acMsisdnActivityRepository.findAllByKeyAndAndKey2In(s, yearWeeks);
            Utility.mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            List<CdrShortModel> cdrShortModels = Utility.cdrShortModelMapper(acMsisdnActivityEntities, writeToFileFiltered, "cdr-by-msisdn-" + s);
            allData.addAll(cdrShortModels);
        });
        return allData;
    }

    @Override
    public List<CdrShortModel> filterCdrByCgi(String fromDateTimeStr,
                                              String toDateTimeStr,
                                              List<String> cgis,
                                              boolean writeToFileFiltered) {
        List<Long> yearWeeks = Utility.getWeeksBetween2Dates(fromDateTimeStr, toDateTimeStr);
        List<AcCgiActivityEntity> acCgiActivityEntities = new ArrayList<>();

        for (String cgi : cgis) {
            List<AcCgiActivityEntity> acCgiActivityEntityList = acCgiActivityRepository.findAllByKeyAndAndKey2In(cgi, yearWeeks);
            acCgiActivityEntities.addAll(acCgiActivityEntityList);
        }

        Utility.mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        List<String> values = new ArrayList<>();
        for (AcCgiActivityEntity acCgiActivityEntity : acCgiActivityEntities) {
            values.add(acCgiActivityEntity.getValue());
        }

        List<CdrShortModel> cdrShortModels = Utility.cdrShortModelMapper(values);
        if (writeToFileFiltered) Utility.cdrCsvFile(cdrShortModels, "cgi");
        return cdrShortModels;
    }

    public List<CdrShortModel> filterCdrWithMsisdnByCgi(String fromDateTimeStr,
                                                        String toDateTimeStr,
                                                        String cgiA,
                                                        String cgiB,
                                                        String msisdnA,
                                                        String msisdnB,
                                                        boolean writeToFileOriginal,
                                                        boolean writeToFileFiltered) {
        List<Long> yearWeeks = Utility.getWeeksBetween2Dates(fromDateTimeStr, toDateTimeStr);

        LocalDateTime from = LocalDateTime.parse(fromDateTimeStr, Utility.formatter);
        LocalDateTime to = LocalDateTime.parse(toDateTimeStr, Utility.formatter);

        List<CdrShortModel> cdrShortModels = new ArrayList<>();
        List<CdrShortModel> cdrShortModelAs = filterCdrByMsisdn(yearWeeks, msisdnA, writeToFileOriginal);
        List<CdrShortModel> cdrShortModelBs = filterCdrByMsisdn(yearWeeks, msisdnB, writeToFileOriginal);
        cdrShortModels.addAll(cdrShortModelAs);
        cdrShortModels.addAll(cdrShortModelBs);

        List<CdrShortModel> filteredCdr = new ArrayList<>();
        for (CdrShortModel cdrShortModel : cdrShortModels) {
            if (from.isBefore(cdrShortModel.getStartLocalDateTime()) && to.isAfter(cdrShortModel.getStartLocalDateTime())) {
                if ((msisdnA.contains(cdrShortModel.getMsisdnA()) && cgiA.contains(cdrShortModel.getCgiA())) ||
                     (msisdnB.contains(cdrShortModel.getMsisdnB()) && cgiB.contains(cdrShortModel.getCgiB()))){
                    filteredCdr.add(cdrShortModel);
                }
            }
        }

        if (writeToFileFiltered) Utility.cdrCsvFile(filteredCdr, "filtered-by-msisdn-cgi");

        return filteredCdr;
    }


    public Map<String, String> getAllTargetAndMsisdn() {
        Map<String, String> targetMsisdn = new HashMap<>();
        tpTargetRepository.findAll()
                          .forEach(tpTargetEntity -> {
                              if (tpTargetEntity.getColumn1()
                                                .equals("Name")) {
                                  List<TpTargetMsisdnEntity> msisdns = tpTargetMsisdnRepository.findByKey(tpTargetEntity.getKey());
                                  msisdns.forEach(tpTargetMsisdnEntity -> {
                                      String targetName = tpTargetEntity.getValue();
                                      String msisdnValue = tpTargetMsisdnEntity.getColumn1();
                                      log.info("{} : {}", targetName, msisdnValue);
                                      String existingMsisdn = targetMsisdn.get(targetName);
                                      if (existingMsisdn != null) {
                                          msisdnValue = existingMsisdn + ", " + msisdnValue;
                                      }
                                      targetMsisdn.put(targetName, msisdnValue);
                                  });
                              }
                          });

        return targetMsisdn;
    }
}
