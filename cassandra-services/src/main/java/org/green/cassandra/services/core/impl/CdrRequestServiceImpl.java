package org.green.cassandra.services.core.impl;

import org.green.cassandra.entity.data.request.AcCdrRequestStatusEntity;
import org.green.cassandra.repository.data.request.AcCdrRequestStatusRepository;
import org.green.cassandra.services.core.CdrRequestService;
import org.green.cassandra.services.core.model.dataretention.AcCdrRequestStatusModel;
import org.javatuples.Quintet;
import org.javatuples.Tuple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CdrRequestServiceImpl implements CdrRequestService {

    @Autowired
    private AcCdrRequestStatusRepository acCdrRequestStatusRepository;

    @Override
    public List<AcCdrRequestStatusModel> getAllCdrRequestStatuses() {
        List<AcCdrRequestStatusEntity> entities = acCdrRequestStatusRepository.findAll();
        Map<Tuple, AcCdrRequestStatusModel> data = new HashMap<>();
        for (AcCdrRequestStatusEntity entity : entities) {
            Tuple tuple = new Quintet(entity.getKey(),
                                      entity.getKey2(),
                                      entity.getKey3(),
                                      entity.getKey4(),
                                      entity.getKey5());

            AcCdrRequestStatusModel record = data.get(tuple);
            if (record == null) {
                record = new AcCdrRequestStatusModel(entity.getKey(),
                                                     entity.getKey2(),
                                                     entity.getKey3(),
                                                     entity.getKey4(),
                                                     entity.getKey5());
                data.put(tuple, record);
            }
            record.addColumnValue(entity.getColumn1(), entity.getValue());
        }

        return new ArrayList<>(data.values());
    }
}
