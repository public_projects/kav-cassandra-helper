package org.green.cassandra.services.core.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.green.cassandra.entity.ImeiTypeAllocationCodeEntity;
import org.green.cassandra.repository.ImeiTypeAllocationCodeRepository;
import org.green.cassandra.services.core.DeviceDetailService;
import org.green.cassandra.services.model.DeviceDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;


@Service
public class DeviceDetailServiceImpl implements DeviceDetailService {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ImeiTypeAllocationCodeRepository imeiTypeAllocationCodeRepository;

    @Override
    public List<ImeiTypeAllocationCodeEntity> getAllDeviceDetail() {
        List<ImeiTypeAllocationCodeEntity> imeiTypeAllocationCodeEntities = imeiTypeAllocationCodeRepository.findAll();
        log.info("Result size: {}", imeiTypeAllocationCodeEntities.size());
        return imeiTypeAllocationCodeEntities;
    }

    @Override
    public boolean writeToDB(String inputFile) {
        AtomicLong totalWrittenRecords = new AtomicLong(0l);
        try {
            String userDirectory = System.getProperty("user.dir");
            String fileSeparator = FileSystems.getDefault()
                                              .getSeparator();
            Map<String, Map<String, String>> deviceDetailMap = objectMapper.readValue(new File(userDirectory + fileSeparator + inputFile), Map.class);
            log.info("Data size: {} to be written to AC_ImeiTypeAllocationCode", deviceDetailMap != null ? deviceDetailMap.size() : null);

            List<ImeiTypeAllocationCodeEntity> batchImeiTypeAllocationCodeEntities = new ArrayList<>(1000);

            Set<String> keys = deviceDetailMap.keySet();
            Iterator<String> keysIt = keys.iterator();
            while(keysIt.hasNext()){
                String key = keysIt.next();
                Map<String, String> deviceDetail = deviceDetailMap.get(key);
                log.debug("key: {} manufacturer: {} model: {}", key, deviceDetail.get("manufacturer"), deviceDetail.get("model"));

                ImeiTypeAllocationCodeEntity imeiTypeAllocationCodeEntity1 = new ImeiTypeAllocationCodeEntity();
                imeiTypeAllocationCodeEntity1.setKey(key);
                imeiTypeAllocationCodeEntity1.setColumn1("manufacturer");
                imeiTypeAllocationCodeEntity1.setValue(deviceDetail.get("manufacturer"));
                batchImeiTypeAllocationCodeEntities.add(imeiTypeAllocationCodeEntity1);

                ImeiTypeAllocationCodeEntity imeiTypeAllocationCodeEntity2 = new ImeiTypeAllocationCodeEntity();
                imeiTypeAllocationCodeEntity2.setKey(key);
                imeiTypeAllocationCodeEntity2.setColumn1("model");
                imeiTypeAllocationCodeEntity2.setValue(deviceDetail.get("model"));
                batchImeiTypeAllocationCodeEntities.add(imeiTypeAllocationCodeEntity2);

                if (batchImeiTypeAllocationCodeEntities.size() == 1000) {
                    log.info("Batch of {} saved!, Total of {}", batchImeiTypeAllocationCodeEntities.size(), totalWrittenRecords.addAndGet(batchImeiTypeAllocationCodeEntities.size()));
                    imeiTypeAllocationCodeRepository.saveAll(batchImeiTypeAllocationCodeEntities);
                    batchImeiTypeAllocationCodeEntities = new ArrayList<>(1000);
                }
            }

            imeiTypeAllocationCodeRepository.saveAll(batchImeiTypeAllocationCodeEntities);
            log.info("Batch of {} saved!, Total of {}",
                     batchImeiTypeAllocationCodeEntities.size(),
                     totalWrittenRecords.addAndGet(batchImeiTypeAllocationCodeEntities.size()));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Completed! {} inserted records.", totalWrittenRecords.get() / 2);
        return false;
    }
}
