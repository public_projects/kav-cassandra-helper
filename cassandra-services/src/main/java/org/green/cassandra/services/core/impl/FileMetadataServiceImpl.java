package org.green.cassandra.services.core.impl;

import org.green.cassandra.entity.file.FileMetadataEntity;
import org.green.cassandra.repository.file.FileMetadataRepository;
import org.green.cassandra.services.model.FileMetadataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FileMetadataServiceImpl {

    private FileMetadataRepository fileMetadataRepository;

    @Autowired
    public FileMetadataServiceImpl(FileMetadataRepository fileMetadataRepository){
        this.fileMetadataRepository = fileMetadataRepository;
    }

    public Collection<FileMetadataModel> getAll() {
        Map<String, FileMetadataModel> data = new HashMap<>();
        List<FileMetadataEntity> allResults = this.fileMetadataRepository.findAll();
        for (FileMetadataEntity entity: allResults) {
            String key = entity.getKey();
            FileMetadataModel model = data.get(key);
            if (model == null) model = new FileMetadataModel(key);
            setValue(entity.getColumn1(), entity.getValue(), model);
            data.put(key, model);
        }
        return data.values();
    }

    public FileMetadataModel getByKey(String key) {
        List<FileMetadataEntity> allResults = this.fileMetadataRepository.findByKey(key);
        if (allResults == null || allResults.isEmpty()) return null;
        FileMetadataModel model = new FileMetadataModel(key);
        for (FileMetadataEntity entity: allResults) {
            setValue(entity.getColumn1(), entity.getValue(), model);
        }
        return model;
    }

    void setValue(String key, String value, FileMetadataModel model) {
        key = key.toUpperCase();
        switch(key) {
            case "FORMAT":
                model.setFormat(value);
                break;
            case "PERCENTAGE":
                model.setPercentage(Integer.valueOf(value));
                break;
            case "PID":
                model.setPid(value);
                break;
            case "STATUS":
                model.setStatus(value);
                break;
            case "FILESIZE":
                model.setFileSize(Integer.valueOf(value));
        }
    }
}
