package org.green.cassandra.services.core.impl;

import org.green.cassandra.entity.InterceptContentLocatorEntity;
import org.green.cassandra.entity.McngToIpfEntity;
import org.green.cassandra.repository.InterceptContentLocatorRepository;
import org.green.cassandra.repository.McngToIpfRepository;
import org.green.cassandra.services.core.McngIdService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class McngIdServiceImpl implements McngIdService {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private InterceptContentLocatorRepository interceptContentLocatorRepository;

    @Autowired
    private McngToIpfRepository mcngToIpfRepository;

    @Override
    public List<Long> getTargetMcngId() {
        List<McngToIpfEntity> mcngToIpfEntities = mcngToIpfRepository.findAll();

        List<Long> targetMcngIds = new ArrayList<>(mcngToIpfEntities.size());
        mcngToIpfEntities.forEach(mcngToIpfEntity -> targetMcngIds.add(Long.parseLong(mcngToIpfEntity.getKey3())));

        return targetMcngIds;
    }

    @Override
    public List<Long> getAllMcngId() {
        List<InterceptContentLocatorEntity> interceptContentLocatorEntities = interceptContentLocatorRepository.findAll();

        List<Long> callMcngIds = new ArrayList<>(interceptContentLocatorEntities.size());
        interceptContentLocatorEntities.forEach(interceptContentLocatorEntity -> callMcngIds.add(Long.parseLong(interceptContentLocatorEntity.getKey())));

        return callMcngIds;
    }
}
