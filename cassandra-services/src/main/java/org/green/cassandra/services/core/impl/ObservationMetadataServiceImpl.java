package org.green.cassandra.services.core.impl;

import org.green.cassandra.entity.analysis.ObservationMetadataEntity;
import org.green.cassandra.repository.analysis.ObservationMetadataRepository;
import org.green.cassandra.services.model.ObservationMetadataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ObservationMetadataServiceImpl {

    private final ObservationMetadataRepository repository;

    @Autowired
    public ObservationMetadataServiceImpl(ObservationMetadataRepository fileMetadataRepository){
        this.repository = fileMetadataRepository;
    }

    public ObservationMetadataModel getById(String key) {
        List<ObservationMetadataEntity> allResults =  this.repository.findByKey(key);
        if (allResults == null || allResults.isEmpty()) return null;

        ObservationMetadataModel model = new ObservationMetadataModel(key);
        for (ObservationMetadataEntity entity: allResults) {
            setValue(entity.getColumn1(), entity.getValue(), model);
        }

        return model;
    }

    public Collection<ObservationMetadataModel> getAll() {
        Map<String, ObservationMetadataModel> data = new HashMap<>();
        List<ObservationMetadataEntity> allResults = this.repository.findAll();
        for (ObservationMetadataEntity entity: allResults) {
            String key = entity.getKey();
            ObservationMetadataModel model = data.get(key);
            if (model == null) model = new ObservationMetadataModel(key);
            setValue(entity.getColumn1(), entity.getValue(), model);
            data.put(key, model);
        }
        return data.values();
    }

    void setValue(String key, String value, ObservationMetadataModel model) {
        key = key.toLowerCase();
        switch(key) {
            case "creationdate":
                model.setCreationDate(Long.parseLong(value));
                break;
            case "enddate":
                model.setEndDate(Long.parseLong(value));
                break;
            case "geohashes":
                model.setGeoHashes(value);
                break;
            case "modificationdate":
                model.setModificationDate(Long.parseLong(value));
                break;
            case "name":
                model.setName(value);
                break;
            case "ownerid":
                model.setOwnerId(value);
        }
    }
}
