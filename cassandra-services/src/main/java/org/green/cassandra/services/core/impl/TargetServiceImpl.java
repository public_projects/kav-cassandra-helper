package org.green.cassandra.services.core.impl;

import org.green.cassandra.entity.TpTargetEntity;
import org.green.cassandra.repository.TpTargetRepository;
import org.green.cassandra.services.core.TargetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.query.CassandraPageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


@Service
public class TargetServiceImpl implements TargetService {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private TpTargetRepository tpTargetRepository;

    @Override
    public long getTargetCount() {
        return tpTargetRepository.count();
    }

    @Override
    public TpTargetEntity getTargetById(String uuid) {
        List<TpTargetEntity> tpTargetEntityList = tpTargetRepository.findByKey(uuid);
        tpTargetEntityList.forEach(tpTargetEntity -> log.info("tpTarget: {}", tpTargetEntity.getColumn1()));
        return null;
    }

    @Override
    public TpTargetEntity saveTarget(TpTargetEntity tpTargetEntity) {
        return tpTargetRepository.save(tpTargetEntity);
    }

    @Override
    public TpTargetEntity removeTarget(String uuid) {
        return null;
    }

    public TpTargetEntity getTargetByName(String targetName) {
        int recordCount = 1000;
        Slice<TpTargetEntity> tpTargetEntitySlice = this.tpTargetRepository.findAll(CassandraPageRequest.first(recordCount));
        AtomicInteger pageCount = new AtomicInteger(0);

        Pageable pageable;
        TpTargetEntity foundTarget;
        if (tpTargetEntitySlice != null && tpTargetEntitySlice.hasContent()) {
            log.debug("pageNumber: {}, pageSize:{}", pageCount.get(), recordCount);
            foundTarget = checkForGivenTarget(tpTargetEntitySlice, targetName);

            if (foundTarget != null) {
                return foundTarget;
            }
        }

        while (tpTargetEntitySlice.hasNext()) {
            Pageable nextPageable = tpTargetEntitySlice.nextPageable();
            tpTargetEntitySlice = this.tpTargetRepository.findAll(nextPageable);
            log.debug("pageNumber: {}, pageSize:{}", pageCount.incrementAndGet(), recordCount);
            foundTarget = checkForGivenTarget(tpTargetEntitySlice, targetName);

            if (foundTarget != null) {
                return foundTarget;
            }
        }

        return null;
    }

    private TpTargetEntity checkForGivenTarget(Slice<TpTargetEntity> tpTargetEntitySlice,  String targetName) {
        for (TpTargetEntity tpTargetEntity : tpTargetEntitySlice.getContent()) {

            if (tpTargetEntity.getColumn1()
                              .equalsIgnoreCase("Name")) {

                if (targetName.equalsIgnoreCase(tpTargetEntity.getValue())) {
                    log.info("Found {}", tpTargetEntity.getValue());
                    return tpTargetEntity;
                }
            }
        }
        return null;
    }
}
