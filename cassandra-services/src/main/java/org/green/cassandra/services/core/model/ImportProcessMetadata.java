package org.green.cassandra.services.core.model;


import org.green.cassandra.entity.analysis.ImportProcessMetadataEntity;

import java.util.HashMap;
import java.util.Map;

public class ImportProcessMetadata {
    public static final String INVESTIGATION_ID = "INVESTIGATION_ID";
    public static final String INVESTIGATION_TYPE = "INVESTIGATION_TYPE";
    public static final String PARENT_ID = "PARENT_ID";
    public static final String PID = "PID";
    private Map<String, String> values;

    private String id;
    private String type;
    private String parentId;
    private String pid;

    public ImportProcessMetadata(String key) {
        this.values = new HashMap<>();
    }

    public void setValue(ImportProcessMetadataEntity entity) {
        String column1 = entity.getColumn1();
        String value = entity.getValue();

        switch (column1) {
            case INVESTIGATION_ID:
                id = value;
                break;
            case INVESTIGATION_TYPE:
                type = value;
                break;
            case PARENT_ID:
                parentId = value;
                break;
            case PID:
                pid = value;
                break;
            default:
                values.put(column1, value);
        }
    }

    @Override
    public String toString() {
        String header = "id;type;parentId;pid;values" + System.lineSeparator();
        String result = id + ";" + type + ";" + parentId + ";" + pid+ ";" + values + System.lineSeparator();
        return header + result;
    }
}
