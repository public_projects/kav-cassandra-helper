package org.green.cassandra.services.core.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InvestigationManagement implements Serializable {


    public static final String HEADER = getHeader();

    private String key;

    private String column1;

    public long creationDate;
    public String investigationName;
    public String investigationType;
    public String investigationStatus;
    public String description;
    public List<String> observationList;

    public InvestigationManagement(){
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.append(key)
                 .append(";")
                 .append(creationDate)
                 .append(";")
                 .append(investigationType)
                 .append(";")
                 .append(investigationName)
                 .append(";")
                 .append(investigationStatus)
                 .append(";")
                 .append(description)
                 .append(";")
                 .append(observationList)
                 .append(System.lineSeparator()).toString();
    }

    public static String getHeader() {
        StringBuilder sb = new StringBuilder();
        sb.append("[InvestigationManagement_v2]").append(System.lineSeparator());
        sb.append("key;createdTime;investigationType;investigationName;investigationStatus;description;observationList");
        return sb.toString();
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getColumn1() {
        return column1;
    }

    public void setColumn1(String column1) {
        this.column1 = column1;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    public String getInvestigationName() {
        return investigationName;
    }

    public void setInvestigationName(String investigationName) {
        this.investigationName = investigationName;
    }

    public String getInvestigationType() {
        return investigationType;
    }

    public void setInvestigationType(String investigationType) {
        this.investigationType = investigationType;
    }

    public String getInvestigationStatus() {
        return investigationStatus;
    }

    public void setInvestigationStatus(String investigationStatus) {
        this.investigationStatus = investigationStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getObservationList() {
        return observationList;
    }

    public void setObservationList(List<String> observationList) {
        this.observationList = observationList;
    }
}
