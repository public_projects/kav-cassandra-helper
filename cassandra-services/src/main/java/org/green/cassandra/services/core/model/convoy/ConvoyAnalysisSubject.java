package org.green.cassandra.services.core.model.convoy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class ConvoyAnalysisSubject implements Serializable {
    private String investigationId;
    private String investigationName;
    private String investigationStatus;
    private List<ConvoyAnalysisSubjectTarget> observedTargets = new ArrayList<>();
    private List<ConvoyAnalysisSubjectIdentity> identities = new ArrayList<>();
    private Long creationDate;

    public void addObservedTarget(ConvoyAnalysisSubjectTarget observedTarget) {
        this.observedTargets.add(observedTarget);
    }

    public void addObservedIdentities(List<ConvoyAnalysisSubjectIdentity> identities){
        this.identities = identities;
    }

    public List<ConvoyAnalysisSubjectTarget> getObservedTargets() {
        return observedTargets;
    }

    public List<ConvoyAnalysisSubjectIdentity> getIdentities() {
        return identities;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public String getInvestigationId() {
        return investigationId;
    }

    public void setInvestigationId(String investigationId) {
        this.investigationId = investigationId;
    }

    public String getInvestigationName() {
        return investigationName;
    }

    public void setInvestigationName(String investigationName) {
        this.investigationName = investigationName;
    }

    public String getInvestigationStatus() {
        return investigationStatus;
    }

    public void setInvestigationStatus(String investigationStatus) {
        this.investigationStatus = investigationStatus;
    }

    @Override
    public String toString() {
        return "ConvoyAnalysisSubject{" +
                "investigationId='" + investigationId + '\'' +
                ", investigationName='" + investigationName + '\'' +
                ", investigationStatus='" + investigationStatus + '\'' +
                ", observedTargets=" + observedTargets +
                ", identities=" + identities +
                ", creationDate=" + creationDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConvoyAnalysisSubject)) return false;
        ConvoyAnalysisSubject that = (ConvoyAnalysisSubject) o;
        return Objects.equals(getInvestigationId(), that.getInvestigationId()) &&
               Objects.equals(getInvestigationName(), that.getInvestigationName()) &&
               Objects.equals(getObservedTargets(), that.getObservedTargets()) &&
               Objects.equals(getIdentities(), that.getIdentities()) &&
               Objects.equals(getCreationDate(), that.getCreationDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getInvestigationId(),
                            getInvestigationName(),
                            getObservedTargets(),
                            getIdentities(),
                            getCreationDate());
    }
}
