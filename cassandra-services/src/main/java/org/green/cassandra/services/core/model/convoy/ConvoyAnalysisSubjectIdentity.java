package org.green.cassandra.services.core.model.convoy;

import java.io.Serializable;
import java.util.Objects;

public class ConvoyAnalysisSubjectIdentity implements Serializable {
    private String identityId;
    private String identityCaption;
    private String identityNature;
    private String serviceName;

    public ConvoyAnalysisSubjectIdentity() {
    }

    public ConvoyAnalysisSubjectIdentity(String identityId, String identityCaption, String identityNature, String serviceName){
        this.identityId = identityId;
        this.identityCaption = identityCaption;
        this.identityNature = identityNature;
        this.serviceName = serviceName;
    }

    public String getIdentityCaption() {
        return identityCaption;
    }

    public void setIdentityCaption(String identityCaption) {
        this.identityCaption = identityCaption;
    }

    public String getIdentityNature() {
        return identityNature;
    }

    public void setIdentityNature(String identityNature) {
        this.identityNature = identityNature;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getIdentityId() {
        return identityId;
    }

    public void setIdentityId(String identityId) {
        this.identityId = identityId;
    }


    @Override
    public String toString() {
        return "ConvoyAnalysisSubjectIdentity{" +
                "identityId='" + identityId + '\'' +
                ", identityCaption='" + identityCaption + '\'' +
                ", identityNature='" + identityNature + '\'' +
                ", serviceName='" + serviceName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConvoyAnalysisSubjectIdentity)) return false;
        ConvoyAnalysisSubjectIdentity that = (ConvoyAnalysisSubjectIdentity) o;
        return Objects.equals(getIdentityId(), that.getIdentityId()) && Objects.equals(getIdentityCaption(), that.getIdentityCaption()) &&
               Objects.equals(getIdentityNature(), that.getIdentityNature()) && Objects.equals(getServiceName(), that.getServiceName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdentityId(), getIdentityCaption(), getIdentityNature(), getServiceName());
    }
}
