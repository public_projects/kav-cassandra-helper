package org.green.cassandra.services.core.model.convoy;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class ConvoyAnalysisSubjectTarget implements Serializable {
    private String targetId;
    private String name;
    private List<ConvoyAnalysisSubjectIdentity> targetIdentities;

    public ConvoyAnalysisSubjectTarget(){}

    public ConvoyAnalysisSubjectTarget(String targetId,
                                       String name,
                                       List<ConvoyAnalysisSubjectIdentity> convoyAnalysisSubjectIdentities) {
        this.targetId = targetId;
        this.name = name;
        this.targetIdentities = convoyAnalysisSubjectIdentities;
    }

    public List<ConvoyAnalysisSubjectIdentity> getTargetIdentities() {
        return targetIdentities;
    }

    public String getTargetId() {
        return targetId;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "ConvoyAnalysisSubjectTarget{" +
                "targetId='" + targetId + '\'' +
                ", name='" + name + '\'' +
                ", targetIdentities=" + targetIdentities +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConvoyAnalysisSubjectTarget)) return false;
        ConvoyAnalysisSubjectTarget that = (ConvoyAnalysisSubjectTarget) o;
        return Objects.equals(getTargetId(), that.getTargetId()) && Objects.equals(getName(), that.getName()) && Objects.equals(
                getTargetIdentities(),
                that.getTargetIdentities());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTargetId(), getName(), getTargetIdentities());
    }
}
