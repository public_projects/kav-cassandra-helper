package org.green.cassandra.services.core.model.dataretention;

import org.green.cassandra.services.util.DateConvertorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AcCdrRequestStatusModel {
    private static final Logger logger = LoggerFactory.getLogger(AcCdrRequestStatusModel.class);

    public static final String HEADERS = "id;fromTimeId;toTimeId;type;context;mapValues";

    public static final String REQUEST_END = "REQUEST_END";
    private static final String REQUEST_ID = "REQUEST_ID";
    private static final String REQUEST_START = "REQUEST_START";
    private static final String STATUS = "STATUS";
    public static final List<String> allowedKeys;

    static {
        allowedKeys = new ArrayList<>();
        allowedKeys.add(REQUEST_END);
        allowedKeys.add(REQUEST_ID);
        allowedKeys.add(REQUEST_START);
        allowedKeys.add(STATUS);
    }

    private final String id;

    private final long fromTimeId;

    private final long toTimeId;

    private final String type;

    private final String context;

    private final Map<String, String> columnValueMap;

    public AcCdrRequestStatusModel(String key, long key2, long key3, String key4, String key5) {
        this.id = key;
        this.fromTimeId = key2;
        this.toTimeId = key3;
        this.type = key4;
        this.context = key5;
        this.columnValueMap = new HashMap<>();
    }

    public void addColumnValue(String columnName, String columnValue) {
        if (!allowedKeys.contains(columnName)) {
            logger.trace("Not supported columnName: {}", columnName);
            return;
        }

        columnValueMap.put(columnName, columnValue);
    }

    public String getId() {
        return id;
    }

    public long getFromTimeId() {
        return fromTimeId;
    }

    public long getToTimeId() {
        return toTimeId;
    }

    public String getType() {
        return type;
    }

    public String getContext() {
        return context;
    }

    public Map<String, String> getColumnValueMap() {
        return columnValueMap;
    }

    public String getColumnValueMapStr(){
        StringBuilder sb = new StringBuilder();
        sb.append('{');
        for (Map.Entry<String,String> entry : columnValueMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            sb.append(key);
            sb.append('=');
            sb.append(value);
            if (key.equals(REQUEST_START) || key.equals(REQUEST_END)){
                try {
                    Long epochTime = Long.parseLong(value);
                    sb.append(" (").append(DateConvertorUtil.getDateTime(epochTime)).append(")");
                } catch (NumberFormatException nfe) {

                }
            }
            sb.append(',');
        }
        sb.setCharAt(sb.length() - 1, '}');
        return sb.toString();
    }

    @Override
    public String toString() {
        return id + ";" +
                fromTimeId + " (" + DateConvertorUtil.getDateTime(fromTimeId) + ")" + ";" +
                toTimeId + " (" + DateConvertorUtil.getDateTime(toTimeId) + ")" + ";" +
                type + ";" +
                context + ";" +
                getColumnValueMapStr();
    }
}
