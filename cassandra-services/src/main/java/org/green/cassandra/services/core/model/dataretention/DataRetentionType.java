package org.green.cassandra.services.core.model.dataretention;

public enum DataRetentionType {
    UNKNOWN("UNKNOWN"),
    CALL("CALL"),
    IP("IP"),
    GEO("GEO");

    private final String id;

    DataRetentionType(String id) {
        this.id = id;
    }

    public static DataRetentionType getType(final String id) {
        for (DataRetentionType type : values())
            if (type.id.equalsIgnoreCase(id))
                return type;

        return UNKNOWN;
    }

    @Override
    public String toString() {
        return this.id;
    }
}
