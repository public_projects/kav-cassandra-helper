package org.green.cassandra.services.core.model.dataretention;

import java.io.Serializable;


public class RequestHistorySignature implements Serializable {
    private static final long serialVersionUID = 1L;
    private DataRetentionType dataRetentionType;
    private String cdrId;
    private long fromDateId;
    private long toDateId;
    private String requestType;
    private RequestStatus status;
    private long requestStartId;
    private long requestEndId;
    private String ipdrId;
    private String provider;

    public RequestHistorySignature(String cdrId, long fromDateId, long toDateId, String requestType, RequestStatus status,
                                   long requestStartId, long requestEnd) {
        this.dataRetentionType = DataRetentionType.CALL;
        this.cdrId = cdrId;
        this.fromDateId = fromDateId;
        this.toDateId = toDateId;
        this.requestType = requestType;
        this.status = status;
        this.requestStartId = requestStartId;
        this.requestEndId = requestEnd;
        this.ipdrId = "";
        this.provider = "";
    }

    public RequestHistorySignature(String ipdrId, String provider, long fromDateId, long toDateId, String requestType,
                                   RequestStatus status, long requestStartId, long requestEnd) {
        this.dataRetentionType = DataRetentionType.IP;
        this.cdrId = "0";
        this.fromDateId = fromDateId;
        this.toDateId = toDateId;
        this.requestType = requestType;
        this.status = status;
        this.requestStartId = requestStartId;
        this.requestEndId = requestEnd;
        this.ipdrId = ipdrId;
        this.provider = provider;
    }

    public DataRetentionType getDataRetentionType() {
        return dataRetentionType;
    }

    public String getCdrId() {
        return cdrId;
    }

    public long getFromDateId() {
        return fromDateId;
    }

    public long getToDateId() {
        return toDateId;
    }

    public String getRequestType() {
        return requestType;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public long getRequestStartId() {
        return requestStartId;
    }

    public long getRequestEndId() {
        return requestEndId;
    }

    public String getIpdrId() {
        return ipdrId;
    }

    public String getProvider() {
        return provider;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RequestHistorySignature that = (RequestHistorySignature) o;

        return dataRetentionType == that.dataRetentionType &&
                cdrId == that.cdrId &&
                fromDateId == that.fromDateId &&
                toDateId == that.toDateId &&
                requestType.equals(that.requestType) &&
                status.equals(that.status) &&
                requestStartId == that.requestStartId &&
                requestEndId == that.requestEndId &&
                ipdrId.equals(that.ipdrId) &&
                provider.equals(that.provider);
    }

    @Override
    public int hashCode() {
        int result = dataRetentionType != null ? dataRetentionType.hashCode() : 0;
        result = 31 * result + (int) (fromDateId ^ (fromDateId >>> 32));
        result = 31 * result + (int) (toDateId ^ (toDateId >>> 32));
        result = 31 * result + (requestType != null ? requestType.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (int) (requestStartId ^ (requestStartId >>> 32));
        result = 31 * result + (int) (requestEndId ^ (requestEndId >>> 32));
        result = 31 * result + (ipdrId != null ? ipdrId.hashCode() : 0);
        result = 31 * result + (provider != null ? provider.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RequestHistorySignature{");
        sb.append("dataRetentionType=").append(dataRetentionType);
        sb.append(", cdrId='").append(cdrId).append('\'');
        sb.append(", fromDateId=").append(fromDateId);
        sb.append(", toDateId=").append(toDateId);
        sb.append(", requestType='").append(requestType).append('\'');
        sb.append(", status=").append(status);
        sb.append(", requestStartId=").append(requestStartId);
        sb.append(", requestEndId=").append(requestEndId);
        sb.append(", ipdrId='").append(ipdrId).append('\'');
        sb.append(", provider='").append(provider).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
