package org.green.cassandra.services.core.model.dataretention;

public enum RequestStatus {
    UNKNOWN("UNKNOWN"),
    PENDING("PENDING"),
    SUCCESS("SUCCESS"),
    FAILED("FAILED");

    private final String id;

    RequestStatus(String id) {
        this.id = id;
    }

    public static RequestStatus getType(final String id) {
        for (RequestStatus type : values())
            if (type.id.equalsIgnoreCase(id))
                return type;

        return UNKNOWN;
    }

    @Override
    public String toString() {
        return this.id;
    }
}
