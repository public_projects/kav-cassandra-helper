package org.green.cassandra.services.main;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.green.cassandra.entity.*;
import org.green.cassandra.entity.proximity_analysis.ProximityAnalysisObservationEntity;
import org.green.cassandra.repository.*;
import org.green.cassandra.repository.proximity_analysis.ProximityAnalysisObservationRepository;
import org.green.cassandra.services.astra.AstraService;
import org.green.cassandra.services.core.Utility;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class Main {
    private static final Logger log = LoggerFactory.getLogger(Main.class);
    private static ObjectMapper mapper = new ObjectMapper();
    @Autowired
    private AcCgiActivityRepository acCgiActivityRepository;
    @Autowired
    private TpMsisdnTargetRepository tpMsisdnTargetRepository;
    @Autowired
    private ProximityAnalysisObservationRepository proximityAnalysisObservationRepository;
    @Autowired
    private GeoPureLocationRepository geoPureLocationRepository;

    @Lazy
    @Autowired
    private AstraService astraService;

    @Test
    public void getAstraClient(){

    }

    public void getAllCgiByMsisdn() {
        List<AcCgiActivityEntity> acCgiActivityEntities = acCgiActivityRepository.findAll();

        acCgiActivityEntities.forEach(acCgiActivityEntity -> {
            if (acCgiActivityEntity.getKey()
                                   .equals("")) {
                log.info("MSISDN: {}, date: {}", acCgiActivityEntity.getKey(), acCgiActivityEntity.getKey2());
            }
        });

        log.info("End");
    }

    public void getProximity() {
        List<ProximityAnalysisObservationEntity> proximityAnalysisObservationEntities = proximityAnalysisObservationRepository.findAll();

        proximityAnalysisObservationEntities.forEach(proximityAnalysisObservationEntity -> {
            String proximityAnalysisStr = proximityAnalysisObservationEntity.getValue();
            log.info("{}", proximityAnalysisStr);
            try {
                JsonNode jsonTreeNode = mapper.readTree(proximityAnalysisStr);
                ArrayNode observerEntityNode = (ArrayNode) jsonTreeNode.get("observedIdentities");
                JsonNode observedTarget = jsonTreeNode.get("observedTarget");
                if (observedTarget == null) {
                    observerEntityNode.forEach(jsonNode1 -> log.info("{}", jsonNode1));
//                    JsonNode observedTargetNameJsonObject = observedTarget.get("name");
//                    log.info("TargetName: {}", observedTargetNameJsonObject.asText());
                }

                if (observedTarget != null) {
                    JsonNode observedTargetNameJsonObject = observedTarget.get("name");
//                    log.info("TargetName: {}", observedTargetNameJsonObject.asText());
                }
            }
            catch (Exception a) {
                a.printStackTrace();
            }
        });
    }

    private void updateGeoPureLocationRecord() {
//        this.geoPureLocationRepository.save();
    }

    private void getInvestigationObservations() {
        List<ProximityAnalysisObservationEntity> proximityAnalysisObservationEntities = proximityAnalysisObservationRepository.findAll();

        proximityAnalysisObservationEntities.forEach(proximityAnalysisObservationEntity -> {
            try {
                String jsonValue = proximityAnalysisObservationEntity.getValue();
                Object json = mapper.readValue(jsonValue, Object.class);
                log.info("{}",
                         mapper.writerWithDefaultPrettyPrinter()
                               .writeValueAsString(json));
            }
            catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
    }

//    public void getAllAreaInvestigationToTarget() {
//        List<AreaInvestigationToTargetEntity> acMsisdnActivityEntities = areaInvestigationToTargetRepository.findAll();
//
//        List<String> column1 = new ArrayList<>();
//        column1.add("20200207");
//        column1.add("20200208");
//        column1.add("20200209");
//        column1.add("20200210");
//        column1.add("20200211");
//        column1.add("20200212");
//        column1.add("20200213");
//        column1.add("20200214");
//
//        for (AreaInvestigationToTargetEntity acMsisdnActivityEntity : acMsisdnActivityEntities) {
//            if (acMsisdnActivityEntity.getKey().equals("18437eb0-59e9-11ea-9553-40b034e3cf9d") &&
//                column1.contains(acMsisdnActivityEntity.getColumn1())) {
//                log.info("MSISDN: {}, column1: {}", acMsisdnActivityEntity.getKey(), acMsisdnActivityEntity.getColumn1());
//                log.info("data: {}", acMsisdnActivityEntity.getValue());
//            }
//        }
//
//        log.info("End");
//    }



//    public void getAllGeoPureLocationDataBykeys() {
//        List<GeoPureLocationEntity> geoPureLocationEntities = this.geoPureLocationRepository.findByKeyAndKey2("srextfes",
//                                                                                                              new Long("202021"));
//        geoPureLocationEntities.forEach(geoPureLocationEntity -> {
//            String jsonValue = geoPureLocationEntity.getValue(); // Some json content
//
//            try {
//                JsonNode jsonNodeTree = mapper.readTree(jsonValue);
//                JsonNode sourceJsonNode = jsonNodeTree.get("source");
//                JsonNode instanceJsonNode = sourceJsonNode.get("instance");
//                if (instanceJsonNode != null) {
//                    JsonNode msisdnJsonNode = jsonNodeTree.get("identityType");
//                    if (msisdnJsonNode.asText()
//                                      .equalsIgnoreCase("MSISDN")) {
//                        String msisdn = jsonNodeTree.get("identity")
//                                                    .asText();
//                        List<TpMsisdnTargetEntity> tpMsisdnTargetEntities = tpMsisdnTargetRepository.findByKey(msisdn);
//                        log.info("MSISDN : {}, found records: {}",
//                                 jsonNodeTree.get("identity")
//                                             .asText(),
//                                 tpMsisdnTargetEntities.size());
//
//                    }
//                    log.info("{}", jsonNodeTree.toPrettyString());
//                }
//            }
//            catch (Exception ex) {
//                ex.printStackTrace();
//                System.exit(1);
//            }
//        });
//    }
}
