package org.green.cassandra.services.model;

import java.time.LocalDateTime;

public class CdrShortModel {
    private Long start;
    private String startShort;
    private LocalDateTime startLocalDateTime;
    private String msisdnA;
    private String msisdnB;
    private String imeaA;
    private String imeaB;
    private String imsiA;
    private String imsiB;
    private String cgiA;
    private String cgiB;
    private String callType;
    private String smsContent;
    private String duration;
    private int direction;

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public String getStartShort() {
        return startShort;
    }

    public void setStartShort(String startShort) {
        this.startShort = startShort;
    }

    public LocalDateTime getStartLocalDateTime() {
        return startLocalDateTime;
    }

    public void setStartLocalDateTime(LocalDateTime startLocalDateTime) {
        this.startLocalDateTime = startLocalDateTime;
    }

    public String getMsisdnA() {
        return msisdnA;
    }

    public void setMsisdnA(String msisdnA) {
        this.msisdnA = msisdnA;
    }

    public String getMsisdnB() {
        return msisdnB;
    }

    public void setMsisdnB(String msisdnB) {
        this.msisdnB = msisdnB;
    }

    public String getImeaA() {
        return imeaA;
    }

    public void setImeaA(String imeaA) {
        this.imeaA = imeaA;
    }

    public String getImeaB() {
        return imeaB;
    }

    public void setImeaB(String imeaB) {
        this.imeaB = imeaB;
    }

    public String getImsiA() {
        return imsiA;
    }

    public void setImsiA(String imsiA) {
        this.imsiA = imsiA;
    }

    public String getImsiB() {
        return imsiB;
    }

    public void setImsiB(String imsiB) {
        this.imsiB = imsiB;
    }

    public String getCgiA() {
        return cgiA;
    }

    public void setCgiA(String cgiA) {
        this.cgiA = cgiA;
    }

    public String getCgiB() {
        return cgiB;
    }

    public void setCgiB(String cgiB) {
        this.cgiB = cgiB;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getSmsContent() {
        return smsContent;
    }

    public void setSmsContent(String smsContent) {
        this.smsContent = smsContent;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }
}
