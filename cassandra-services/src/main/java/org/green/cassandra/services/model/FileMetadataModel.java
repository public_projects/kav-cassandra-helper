package org.green.cassandra.services.model;


public class FileMetadataModel {
    public static  String HEADER = "fileId, processId, status, format, percentage";
    private final String key;
    private String format;
    private String pid;
    private String status;
    private Integer percentage;
    private Integer fileSize;
    public FileMetadataModel(String key) {
        this.key = key;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }


    @Override
    public String toString() {
        return key + ", " + pid + ", " + status + ", " + format + "," + percentage;
    }
}
