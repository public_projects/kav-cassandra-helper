package org.green.cassandra.services.model;

import java.util.HashMap;
import java.util.Map;


public class ImeiTypeAllocationCodeModel {
    private Map<String, DeviceDetail> device = new HashMap<>();

    public void addDevice(String imeiPrefix, String columName, String value) {
        DeviceDetail deviceDetail = this.device.get(imeiPrefix);

        if (deviceDetail == null) {
            deviceDetail = new DeviceDetail();
        }

        if (columName.equalsIgnoreCase("manufacturer")) {
            deviceDetail.setManufacturer(value);
        }

        if (columName.equalsIgnoreCase("model") ) {
            deviceDetail.setModel(value);
        }

        this.device.put(imeiPrefix, deviceDetail);
    }

    public Map<String, DeviceDetail> getDevice() {
        return device;
    }
}
