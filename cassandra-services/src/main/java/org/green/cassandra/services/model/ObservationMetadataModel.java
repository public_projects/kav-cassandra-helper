package org.green.cassandra.services.model;

public class ObservationMetadataModel {
    private final String key;
    private long creationDate;
    private long endDate;
    private String geoHashes;
    private long modificationDate;
    private String name;
    private String ownerId;

    public ObservationMetadataModel(String key){
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public String getGeoHashes() {
        return geoHashes;
    }

    public void setGeoHashes(String geoHashes) {
        this.geoHashes = geoHashes;
    }

    public long getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(long modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    @Override
    public String toString() {
        return key + ", " + name;
    }
}
