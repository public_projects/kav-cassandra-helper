package org.green.cassandra.services.model.request;


public class CdrRequestModel {
    private String fromDateTimeStr;
    private String toDateTimeStr;
    private String cgiA;
    private String cgiB;
    private String msisdnA;
    private String msisdnB;
    private String imeiA;
    private String imeiB;
    private String imsiA;
    private String imsiB;
    private boolean writeToFileOriginal;
    private boolean writeToFileFiltered;

    public String getFromDateTimeStr() {
        return fromDateTimeStr;
    }

    public void setFromDateTimeStr(String fromDateTimeStr) {
        this.fromDateTimeStr = fromDateTimeStr;
    }

    public String getToDateTimeStr() {
        return toDateTimeStr;
    }

    public void setToDateTimeStr(String toDateTimeStr) {
        this.toDateTimeStr = toDateTimeStr;
    }

    public String getCgiA() {
        return cgiA;
    }

    public void setCgiA(String cgiA) {
        this.cgiA = cgiA;
    }

    public String getCgiB() {
        return cgiB;
    }

    public void setCgiB(String cgiB) {
        this.cgiB = cgiB;
    }

    public String getMsisdnA() {
        return msisdnA;
    }

    public void setMsisdnA(String msisdnA) {
        this.msisdnA = msisdnA;
    }

    public String getMsisdnB() {
        return msisdnB;
    }

    public void setMsisdnB(String msisdnB) {
        this.msisdnB = msisdnB;
    }

    public String getImeiA() {
        return imeiA;
    }

    public void setImeiA(String imeiA) {
        this.imeiA = imeiA;
    }

    public String getImeiB() {
        return imeiB;
    }

    public void setImeiB(String imeiB) {
        this.imeiB = imeiB;
    }

    public String getImsiA() {
        return imsiA;
    }

    public void setImsiA(String imsiA) {
        this.imsiA = imsiA;
    }

    public String getImsiB() {
        return imsiB;
    }

    public void setImsiB(String imsiB) {
        this.imsiB = imsiB;
    }

    public boolean isWriteToFileOriginal() {
        return writeToFileOriginal;
    }

    public void setWriteToFileOriginal(boolean writeToFileOriginal) {
        this.writeToFileOriginal = writeToFileOriginal;
    }

    public boolean isWriteToFileFiltered() {
        return writeToFileFiltered;
    }

    public void setWriteToFileFiltered(boolean writeToFileFiltered) {
        this.writeToFileFiltered = writeToFileFiltered;
    }
}
