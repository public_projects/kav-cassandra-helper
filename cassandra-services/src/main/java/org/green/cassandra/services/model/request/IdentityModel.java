package org.green.cassandra.services.model.request;

public class IdentityModel {
    private String msisd;
    private String imei;
    private String imsi;

    public String getMsisd() {
        return msisd;
    }

    public void setMsisd(String msisd) {
        this.msisd = msisd;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }
}
