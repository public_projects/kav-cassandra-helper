package org.green.cassandra.services.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.Calendar;

public class DateConvertorUtil {

    public static final DateTimeFormatter dateReadableFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS").withZone(ZoneOffset.UTC);
    private DateTimeFormatter formatter;

    protected DateConvertorUtil(String dateFormat) {
        this.formatter = DateTimeFormatter.ofPattern(dateFormat).withZone(ZoneOffset.UTC);
    }

    public static DateConvertorUtil of(String format) {
        return new DateConvertorUtil(format);
    }

    public static String getDateTime(Long epochTime) {
       return dateReadableFormatter.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(epochTime), ZoneOffset.UTC));
    }

    public String convertEpoch(Long epochTime) {
        LocalDateTime dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(epochTime), ZoneOffset.UTC);
        return formatter.format(dateTime);
    }

    public static long getYearWeek(Long epochTime){
        Instant instant = Instant.ofEpochMilli(epochTime);
        LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        int weekOfYear = ldt.get(WeekFields.ISO.weekOfYear());
        int year = ldt.getYear();
        return new Long(year + "" + weekOfYear);
    }
}
