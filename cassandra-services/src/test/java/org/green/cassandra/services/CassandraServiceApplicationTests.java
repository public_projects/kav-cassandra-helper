package org.green.cassandra.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.green.cassandra.entity.AcMsisdnActivityEntity;
import org.green.cassandra.entity.GeoPureLocationEntity;
import org.green.cassandra.entity.ImeiTypeAllocationCodeEntity;
import org.green.cassandra.entity.TpMsisdnTargetEntity;
import org.green.cassandra.entity.TpTargetEntity;
import org.green.cassandra.entity.analysis.InvestigationManagementEntity;
import org.green.cassandra.entity.analysis.InvestigationMetadataExEntity;
import org.green.cassandra.entity.analysis.ObservationMetadataEntity;
import org.green.cassandra.entity.analysis.convoy.ObservationResultEntity;
import org.green.cassandra.entity.analysis.eba.ObservationParticipationOthersEntity;
import org.green.cassandra.entity.file.FileMetadataEntity;
import org.green.cassandra.entity.generic.AbstractTableType;
import org.green.cassandra.entity.investigation.ActionTraceEntity;
import org.green.cassandra.entity.investigation.InvestigationSearchEntity;
import org.green.cassandra.entity.proximity_analysis.ProximityAnalysisIdentityLocationEntity;
import org.green.cassandra.entity.proximity_analysis.ProximityAnalysisImportDataEntity;
import org.green.cassandra.entity.proximity_analysis.ProximityAnalysisInvestigationEntity;
import org.green.cassandra.entity.proximity_analysis.ProximityAnalysisInvestigationStatusEntity;
import org.green.cassandra.entity.proximity_analysis.model.PaInvestigationModel;
import org.green.cassandra.repository.AcMsisdnActivityRepository;
import org.green.cassandra.repository.AcCgiMsisdnActivityRepository;
import org.green.cassandra.repository.GeoPureLocationRepository;
import org.green.cassandra.repository.TpMsisdnTargetRepository;
import org.green.cassandra.repository.TpTargetMsisdnRepository;
import org.green.cassandra.repository.TpTargetRepository;
import org.green.cassandra.repository.analysis.InvestigationManagementRepository;
import org.green.cassandra.repository.analysis.InvestigationMetadataExRepository;
import org.green.cassandra.repository.analysis.ObservationMetadataRepository;
import org.green.cassandra.repository.analysis.eba.ObservationParticipationOthersRepository;
import org.green.cassandra.repository.file.FileMetadataRepository;
import org.green.cassandra.repository.investigation.ActionTraceRepository;
import org.green.cassandra.repository.proximity_analysis.ProximityAnalysisIdentityLocationRepository;
import org.green.cassandra.repository.proximity_analysis.ProximityAnalysisImportDataRepository;
import org.green.cassandra.repository.proximity_analysis.ProximityAnalysisInvestigationRepository;
import org.green.cassandra.repository.proximity_analysis.ProximityAnalysisInvestigationStatusRepository;
import org.green.cassandra.services.astra.dao.AstraDAO;
import org.green.cassandra.services.core.AnalysisService;
import org.green.cassandra.services.astra.AstraService;
import org.green.cassandra.services.core.CdrActivityService;
import org.green.cassandra.services.core.DeviceDetailService;
import org.green.cassandra.services.core.Utility;
import org.green.cassandra.services.core.impl.FileMetadataServiceImpl;
import org.green.cassandra.services.core.impl.ObservationMetadataServiceImpl;
import org.green.cassandra.services.core.model.InvestigationManagement;
import org.green.cassandra.services.core.model.convoy.ConvoyAnalysisSubject;
import org.green.cassandra.services.core.model.convoy.ConvoyObservationEvent;
import org.green.cassandra.services.model.CdrShortModel;
import org.green.cassandra.services.model.DeviceDetail;
import org.green.cassandra.services.model.FileMetadataModel;
import org.green.cassandra.services.model.ImeiTypeAllocationCodeModel;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.cassandra.core.query.CassandraPageRequest;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.StopWatch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;


@SpringBootTest
//@ActiveProfiles("load_server")
//@ActiveProfiles("DITA4")
//@ActiveProfiles("DCNG2")
@ActiveProfiles("LOCAL")
class CassandraServiceApplicationTests {

    private final Logger log = LoggerFactory.getLogger(CassandraServiceApplicationTests.class);

    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final DateTimeFormatter dateReadableFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
    private static ObjectMapper mapper = new ObjectMapper();
    @Autowired
    private GeoPureLocationRepository geoPureLocationRepository;

    @Autowired
    private AcMsisdnActivityRepository acMsisdnActivityRepository;

    @Autowired
    private ProximityAnalysisIdentityLocationRepository proximityAnalysisIdentityLocationRepository;

    @Autowired
    private TpTargetRepository tpTargetRepository;

    @Autowired
    private TpTargetMsisdnRepository tpTargetMsisdnRepository;

    @Autowired
    private TpMsisdnTargetRepository tpMsisdnTargetRepository;

    @Autowired
    private CdrActivityService cdrActivityService;

    @Autowired
    private DeviceDetailService deviceDetailService;

    @Autowired
    private AnalysisService analysisService;

    @Autowired
    private AcCgiMsisdnActivityRepository acCgiMsisdnActivityRepository;

    @Autowired
    private AstraService astraService;

    @Autowired
    private AstraDAO targetIdentityCountDAOImpl;

    @Autowired
    private FileMetadataServiceImpl fileMetadataService;

    @Autowired
    private ObservationMetadataServiceImpl observationMetadataService;

    @Autowired
    private ObservationParticipationOthersRepository observationParticipationOthersRepository;

    @Autowired
    private ProximityAnalysisImportDataRepository proximityAnalysisImportDataRepository;

    @Autowired
    private ProximityAnalysisInvestigationStatusRepository proximityAnalysisInvestigationStatusRepository;

    @Autowired
    private ProximityAnalysisInvestigationRepository proximityAnalysisInvestigationRepository;

    @Autowired
    private InvestigationManagementRepository investigationManagementRepository;

    @Autowired
    private InvestigationMetadataExRepository investigationMetadataExRepository;

    @Autowired
    private ActionTraceRepository actionTraceRepository;

    @Autowired
    private FileMetadataRepository fileMetadataRepository;

    @Autowired
    private ObservationMetadataRepository observationMetadataRepository;
    @Test
    public void getAllRecordsByInvestigationId() throws Exception {
//        String key = getProximityInvestigationByName("8759026230110106_MSISDN");
//        String key = getProximityInvestigationByName("PRX_MRY_INV_D2_230112065 DRS_DFS");
//        String key = getProximityInvestigationByName("dfs13_local");
//        String key = getProximityInvestigationByName("971512441846784-dfs");
//        String key = getProximityInvestigationByName("588138330206578_MSISDN_DFS");
        String investigationName = "PRX_MRY_INV_D2_230224092";
        String key = getProximityInvestigationByName(investigationName);

//        String id = "3e1a5380-97de-11ed-abc5-2c0da7f630d4";
        String id = key;
        Long dataCount = proximityAnalysisIdentityLocationRepository.count();
        System.err.println("Investigation["+investigationName+"] has "+ dataCount);
        List<ProximityAnalysisIdentityLocationEntity> entities =
                proximityAnalysisIdentityLocationRepository.findAll();

        for (ProximityAnalysisIdentityLocationEntity entity: entities) {
            if (entity.getKey().equalsIgnoreCase(id)){
                Instant instant = Instant.ofEpochMilli(Long.parseLong(entity.getColumn1()));
                LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
                System.err.println(localDateTime + ";" + entity);
            }
        }
    }

    @Test
    public void getAllStatus() {

        List<ProximityAnalysisInvestigationStatusEntity> statuses = proximityAnalysisInvestigationStatusRepository.findAll();

        List<String> expectedObservations = Arrays.asList("b054c110-6f98-11ed-82fb-2c0da7f630d4");

        Map<String, Integer> keys = new HashMap<>();
        AtomicInteger counter = new AtomicInteger();
        System.err.println(ProximityAnalysisInvestigationStatusEntity.getHeaders());
        for (ProximityAnalysisInvestigationStatusEntity entity : statuses) {
            System.err.println(counter.incrementAndGet() + entity.toString());
            Integer value = keys.get(entity.getKey());

            if (value == null) {
                keys.put(entity.getKey(), 1);
                continue;
            }
            keys.put(entity.getKey(), value++);
        }

        for (String key : keys.keySet()) {
            System.err.println(key + ", " + keys.get(key));
        }
    }
    @Test
    public void setNewStatus() {
        long lastUpdatedTime = System.currentTimeMillis();
        ProximityAnalysisInvestigationStatusEntity entity = new ProximityAnalysisInvestigationStatusEntity();
        entity.setKey("b054c110-6f98-11ed-82fb-2c0da7f630d4");
        entity.setColumn1(lastUpdatedTime);
        entity.setValue("{\"lastModifiedDate\":"+ lastUpdatedTime + ",\"status\":\"FAILED\",\"message\":\"\",\"source\":{\"system\":\"mcng\",\"properties\":{\"instance\":\"mcng\",\"agency-id\":\"3\",\"agency-name\":\"ag03\"}}}");

        proximityAnalysisInvestigationStatusRepository.save(entity);
    }

    @Test
    public void getProximityImportData() throws Exception {
//        String investigationId = getProximityInvestigationName("8759710201312481_2");
        String investigationId = getProximityInvestigationByName("593710201312481 IMSI");
        List<ProximityAnalysisImportDataEntity> entities = proximityAnalysisImportDataRepository.findAll();

        List<String> expectedObservations = Arrays.asList(investigationId);

        StringBuilder sb = new StringBuilder();
        sb.append("row").append(AbstractTableType.CSV_DELIMETER).append(ObservationParticipationOthersEntity.getHeaders());
        AtomicInteger counter = new AtomicInteger();
        for (ProximityAnalysisImportDataEntity entity: entities){
            if (expectedObservations.contains(entity.getKey())){
                sb.append(counter.incrementAndGet()).append(AbstractTableType.CSV_DELIMETER).append(entity).append(System.lineSeparator());
            }
        }
        System.err.println(sb);
    }

    @Test
    public void checkLoop() {
        while (!blockingMethod()){
            System.err.println("running ... ");
        }

        System.err.println("result!");
    }

    private static boolean blockingMethod() {
        try {
            Thread.sleep(15_000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        return true;
    }

    @Test
    public void getAllObservationEvents() {
        List<ObservationParticipationOthersEntity> entities = observationParticipationOthersRepository.findAll();

        List<String> expectedObservations = Arrays.asList("dee20086-a273-4c0c-9d8a-eebed4a2c083",
                                                           "ea5aea58-c254-4dab-a726-3f9b9696507d");

        System.err.println("row,"+ObservationParticipationOthersEntity.getHeaders());
        for (ObservationParticipationOthersEntity entity: entities){
            if (expectedObservations.contains(entity.getKey())){
                System.err.println(entity);
            }
        }
    }
    @Test
    public void targetIdentityCountDAOInsertTest() {
//        TargetIdentityCountEntity targetIdentityCountEntity = new TargetIdentityCountEntity();
//        targetIdentityCountEntity.setCount(0);
//        targetIdentityCountEntity.setIdentityType("MSISDN");
//        targetIdentityCountEntity.setMcngId(123456);
//        targetIdentityCountEntity.setName("abc123");
//        targetIdentityCountDAOImpl.insert(targetIdentityCountEntity);

//        targetIdentityCountDAOImpl.fetchAll();

        fileMetadataService.getAll().stream().forEach(fileMetadataModel -> System.err.println(observationMetadataService.getById(
                fileMetadataModel.getPid())));
    }

    @Test
    public void isConnected(){
        String[] columns = {"name", "mcngid", "identitytype", "count"};
        Class[] dataTypes = {String.class, Long.class, String.class, Long.class};
        Object[] values = {"Mary", 12345, "MSISDN", 3};
        astraService.insert("Target_Identity_Count", columns, values, dataTypes);
    }

    public static String getDateFromSeconds(long seconds) {
        LocalDateTime date = LocalDateTime.ofEpochSecond(seconds, 0, ZoneOffset.UTC);
        return formatter.format(date);
    }

    @Test
    public void getDateTimeInMinutes() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        LocalDateTime earlierTime = LocalDateTime.of(2020, 12, 17, 12, 10, 0);
        LocalDateTime currentInstant = LocalDateTime.now();
        System.err.println(dateTimeFormatter.format(currentInstant) + " - " + dateTimeFormatter.format(earlierTime) + " = " +
                           Duration.between(earlierTime, currentInstant).getSeconds());
    }

    //    @Test
    public void saveTarget() {
        TpTargetEntity tpTargetEntity = new TpTargetEntity();
        tpTargetEntity.setKey(UUID.randomUUID().toString());
        tpTargetEntity.setColumn1("name");
        tpTargetEntity.setValue("Abc");
//        TpTargetEntity tpTargetEntity1 = this.tpTargetRepository.save(tpTargetEntity);

//        log.info("tpTargetEntity1 uuid: {}", tpTargetEntity1.getKey());
    }

    //    @Test
    public void deleteTarget() {
        List<TpTargetEntity> targetEntities = this.tpTargetRepository.findByKey("b1caff2f-915d-402c-aade-d21c5d2a7e75");

        targetEntities.forEach(tpTargetEntity -> {
            log.info("target UUID: {}", tpTargetEntity.getKey());
            this.tpTargetRepository.delete(tpTargetEntity);
        });
    }

    @Test
    public void getTargetById() {
//        List<TpTargetEntity> targetEntities = this.tpTargetRepository.findByKey("4f14bf62-b3d4-4a20-a198-876f8a5866ae");
//        List<TpTargetEntity> targetEntities = this.tpTargetRepository.findByKey("b1caff2f-915d-402c-aade-d21c5d2a7e75");
        List<TpTargetEntity> targetEntities = this.tpTargetRepository.findByKey("cb829ada-07a1-44d3-9b22-6683d886f58b");

        targetEntities.forEach(tpTargetEntity -> {
            log.info("target UUID: {}, column: {}, value: {}",
                     tpTargetEntity.getKey(),
                     tpTargetEntity.getColumn1(),
                     tpTargetEntity.getValue());
        });
    }

    //    @Test
    public void testGetTargetByName() {
        String targetName = "gore";
        int recordCount = 5;
        Slice<TpTargetEntity> tpTargetEntitySlice = this.tpTargetRepository.findAll(CassandraPageRequest.first(recordCount));
        AtomicInteger count = new AtomicInteger();

        Pageable pageable;
        TpTargetEntity foundTarget;
        if (tpTargetEntitySlice != null && tpTargetEntitySlice.hasContent()) {
            pageable = tpTargetEntitySlice.getPageable();
            log.error("pageNumber: {}, pageSize:{}", tpTargetEntitySlice.getPageable().getPageNumber(), pageable.getPageSize());

            foundTarget = checkForGivenTarget(tpTargetEntitySlice, count, pageable, targetName);

            if (foundTarget != null) {
                System.err.println("Found target: " + foundTarget.getValue());
            }
        }

        while (tpTargetEntitySlice.hasNext()) {
            Pageable nextPageable = tpTargetEntitySlice.nextPageable();
            tpTargetEntitySlice = this.tpTargetRepository.findAll(nextPageable);

            foundTarget = checkForGivenTarget(tpTargetEntitySlice, count, nextPageable, targetName);
            if (foundTarget != null) {
                System.err.println("Found target: " + foundTarget.getValue());
            }
        }
    }

    private TpTargetEntity checkForGivenTarget(Slice<TpTargetEntity> tpTargetEntitySlice,
                                               AtomicInteger count,
                                               Pageable pageable,
                                               String targetName) {
        for (TpTargetEntity tpTargetEntity : tpTargetEntitySlice.getContent()) {
            // If it is not found get the UUID and mcng id put it in a file.

            if (tpTargetEntity.getColumn1().equalsIgnoreCase("Name")) {
                log.error("{}, Name:{}, target UUID: {}, pageNumber: {}",
                          count.incrementAndGet(),
                          tpTargetEntity.getValue(),
                          tpTargetEntity.getKey(),
                          pageable.getPageNumber());

                if (targetName.equalsIgnoreCase(tpTargetEntity.getValue())) {
                    return tpTargetEntity;
                }
            }
        }
        return null;
    }

    //    @Test
    public void getAllTarget() {
        long targetMsisdnCount = this.tpTargetMsisdnRepository.count();
        log.info("target-msisdn: {}", targetMsisdnCount);

        List<TpTargetEntity> targets = this.tpTargetRepository.findAll();

        targets.forEach(tpTargetEntity -> {
            log.info("target UUID: {}, column: {}, value: {}",
                     tpTargetEntity.getKey(),
                     tpTargetEntity.getColumn1(),
                     tpTargetEntity.getValue());
        });
    }

    public void getAllGeoPureLocationDataFromPage(CassandraRepository cassandraRepository,
                                                  int pageNumber,
                                                  int recordSize,
                                                  String outpufFile) {
        StopWatch watch = new StopWatch();
        watch.start();

        final AtomicInteger countFaulty = new AtomicInteger();
        final AtomicInteger pageCount = new AtomicInteger(pageNumber);
        Pageable pageable = CassandraPageRequest.of(pageNumber, recordSize);
        Slice<GeoPureLocationEntity> geoPureLocationEntities = cassandraRepository.findAll(pageable);
        checkContent(countFaulty, pageCount, geoPureLocationEntities, recordSize, pageable, outpufFile);

        while (geoPureLocationEntities.hasNext()) {
            Pageable nextPage = geoPureLocationEntities.nextPageable();
            geoPureLocationEntities = cassandraRepository.findAll(nextPage);
            checkContent(countFaulty, pageCount, geoPureLocationEntities, recordSize, nextPage, outpufFile);
        }

        watch.stop();
        writeToFile("Completed in: " + watch.getTotalTimeSeconds(), outpufFile);
    }

    public void getAllGeoPureLocationData() {
        StopWatch watch = new StopWatch();
        watch.start();

        int recordCount = 10;
        final AtomicInteger countFaulty = new AtomicInteger();
        final AtomicInteger pageCount = new AtomicInteger();
        Pageable pageable = CassandraPageRequest.first(10);
        Slice<GeoPureLocationEntity> geoPureLocationEntities = this.geoPureLocationRepository.findAll(pageable);
        checkContent(countFaulty, pageCount, geoPureLocationEntities, recordCount, pageable, "D:\\data");

        while (geoPureLocationEntities.hasNext()) {
            Pageable nextPage = geoPureLocationEntities.nextPageable();
            geoPureLocationEntities = this.geoPureLocationRepository.findAll(nextPage);
            checkContent(countFaulty, pageCount, geoPureLocationEntities, recordCount, nextPage, "D:\\data");
        }

        watch.stop();
        writeToFile("Completed in: " + watch.getTotalTimeSeconds(), "D:\\data");
    }

    private void checkContent(AtomicInteger countFaulty,
                              AtomicInteger pageCount,
                              Slice<GeoPureLocationEntity> geoPureLocationEntities,
                              int recordSize,
                              Pageable nextPage,
                              String fileToSave) {
        pageCount.incrementAndGet();
        StringBuilder sb = new StringBuilder();
        geoPureLocationEntities.getContent().forEach(geoPureLocationEntity -> {
            Instant instant;
            ZonedDateTime zdt;
            String jsonValue = geoPureLocationEntity.getValue(); // Some json content
            log.info("{}, {}, {}", geoPureLocationEntity.getKey(), geoPureLocationEntity.getKey2(), geoPureLocationEntity.getColumn1());

            try {
                JsonNode jsonNodeTree = mapper.readTree(jsonValue);
                JsonNode sourceJsonNode = jsonNodeTree.get("source");
                JsonNode instanceJsonNode = sourceJsonNode.get("instance");
                if (instanceJsonNode != null) {
                    int countWrongs = countFaulty.incrementAndGet();
                    log.info("{}, {}", countWrongs, jsonNodeTree.toPrettyString());
                    sb.append("Error, ");
                    sb.append(geoPureLocationEntity.getKey());
                    sb.append(", ");
                    sb.append(geoPureLocationEntity.getKey2());
                    sb.append(", ");
                    sb.append(geoPureLocationEntity.getColumn1());
                    instant = Instant.ofEpochMilli(Long.parseLong(geoPureLocationEntity.getColumn1()));
                    zdt = ZonedDateTime.ofInstant(instant, ZoneOffset.UTC);
                    sb.append(", ");
                    sb.append(dateReadableFormatter.format(zdt));
                    sb.append(geoPureLocationEntity.getColumn1());
                    sb.append(System.lineSeparator());
                    sb.append("Faulty, ");
                    sb.append(countWrongs);
                    sb.append(jsonNodeTree.toPrettyString());
                    sb.append(System.lineSeparator());

                    JsonNode msisdnJsonNode = jsonNodeTree.get("identityType");
                    if (msisdnJsonNode.asText().equalsIgnoreCase("MSISDN")) {
                        String msisdn = jsonNodeTree.get("identity").asText();
                        List<TpMsisdnTargetEntity> tpMsisdnTargetEntities = this.tpMsisdnTargetRepository.findByKey(msisdn);
                        log.info("MSISDN : {}, found records: {}", jsonNodeTree.get("identity").asText(), tpMsisdnTargetEntities.size());
                        StringBuilder sbMsisdn = new StringBuilder();

                        sbMsisdn.append(sb.toString());
                        sbMsisdn.append("MSISDN: " + msisdn + " found: " + tpMsisdnTargetEntities.size());
                        sbMsisdn.append(System.lineSeparator());

                        writeToFile(sbMsisdn.toString(), fileToSave + "_msisdn");
                    }
                    log.info("{}", jsonNodeTree.toPrettyString());
                }
                else {
                    sb.append(geoPureLocationEntity.getKey());
                    sb.append(", ");
                    sb.append(geoPureLocationEntity.getKey2());
                    sb.append(", ");
                    sb.append(geoPureLocationEntity.getColumn1());
                    sb.append(System.lineSeparator());
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                System.exit(1);
            }
        });

        log.info("pageNumber:{}, PageSize:{}, Offset:{}, size: {}, getNumberOfElements: {}, getNumber: {}",
                 nextPage.getPageNumber(),
                 nextPage.getPageSize(),
                 nextPage.getOffset(),
                 geoPureLocationEntities.getSize(),
                 geoPureLocationEntities.getNumberOfElements(),
                 geoPureLocationEntities.getNumber());

        sb.append(nextPage.getPageNumber());
        sb.append(", ");
        sb.append(pageCount.get());
        sb.append(", ");
        int totalRecords = pageCount.get() * recordSize - recordSize + geoPureLocationEntities.getNumberOfElements();
        sb.append(totalRecords);
        sb.append(System.lineSeparator());
        writeToFile(sb.toString(), fileToSave);
    }

    public void writeToFile(String jsonStr, String filePath) {
        File jsonFile = new File(filePath + ".log");
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(jsonFile, true));
            writer.write(jsonStr);
        } catch (IOException ioe) {
            log.error("Failed to write to file: {}", ioe);
        } finally {
            try {
                writer.close();
            } catch (IOException io) {
                // Just ignore
            }
        }
        log.info("Written to {}", jsonFile.getAbsolutePath());
    }

    @Test
    public void testMain() throws Exception {
        LocalDateTime localDateTime = LocalDateTime.now();
//        this.getAllGeoPureLocationDataFromPage(geoPureLocationRepository, 0, 300, "D:\\opt\\" + localDateTime.format(formatter));

        //        getAllCgiByMsisdn();
//        getAllAcMsisdn();
//        getAllAreaInvestigationToTarget();
//        getAllTargetAndMsisdn();
//        getProximity();
//        getInvestigationObservations();
//        getAllGeoPureLocationData();
//        getAllGeoPureLocationDataBykeys();

        //        updateGeoPureLocationRecord();
//        this.mcngIdService.getAllMcngId();
    }

    //    @Test
    public void testGetAllTargetAndMsisdn() {
        Map<String, String> targetMsisdnMap = cdrActivityService.getAllTargetAndMsisdn();
        targetMsisdnMap.forEach((s, s2) -> {
            System.err.println("target: " + s + ", [msisdn: " + s2 + "]");
        });
    }

    //    @Test
    public void testFilterCdrByMsisdn() {
        cdrActivityService.filterCdrByMsisdn("2020-10-31 00:00:00",
                                             "2020-11-05 00:00:00",
                                             Arrays.asList("607240733617155", "600856483322156"),
                                             true);
    }

    @Disabled("Disabled until CustomerService is up!")
    @Test
    public void testFilterCdrByMsisdnParticipant() {
        List<CdrShortModel> results = cdrActivityService.filterCdrWithMsisdnByCgi("2020-11-03 19:06:13",
                                                                                  "2020-11-04 19:06:13",
                                                                                  "219-1-15153-36107",
                                                                                  "219-1-15153-36107",
                                                                                  "607240733617155",
                                                                                  "600856483322156",
                                                                                  false,
                                                                                  false);

        Utility.cdrCsvFile(results, "Filtered-by-msisdn-cgi");
    }

//    @Disabled("Disabled until CustomerService is up!")
    @Test
    public void testFilterCdrByCgi() {
        cdrActivityService.filterCdrByCgi("2021-11-25 00:00:00", "2021-12-01 23:59:00", Arrays.asList("219-1-15153-36107"), true);
    }

    @Test
    public void getWeeksBetween2Dates() {
        List<Long> yearWeeks = Utility.getWeeksBetween2Dates("2020-10-25 00:00:00", "2020-11-02 00:00:00");
        String msisdn = "850490917989565";

        long count = acMsisdnActivityRepository.count();
        System.err.println("count: " + count);
        List<AcMsisdnActivityEntity> msisdnActivityEntityList = acMsisdnActivityRepository.findAllByKeyAndAndKey2In(msisdn, yearWeeks);
        System.err.println(msisdnActivityEntityList.size());
        Utility.cdrShortModelMapper(msisdnActivityEntityList, true, "byDate-msisdn");
    }

    //    @Test
    void comparing2List() {
        List<String> givenGeoHashLs = getUserInputGeoHashList();
        List<String> calculatedGeoHashLs = getCalculatedGeoHashList();
        findNewGeoHashes(givenGeoHashLs, calculatedGeoHashLs);

        final Map<String, Integer> givenGeoHashMap = new HashMap<>();

        givenGeoHashLs.forEach(s -> {
            Integer geoHashCount = givenGeoHashMap.get(s);
            if (geoHashCount == null) {
                givenGeoHashMap.put(s, 1);
            }
            else {
                givenGeoHashMap.put(s, ++geoHashCount);
            }
        });

        log.info("Frequency of geohashes: {}", givenGeoHashMap);
        givenGeoHashMap.toString();

        Set<String> duplicateRemovedSet = new HashSet<String>();
        duplicateRemovedSet.addAll(givenGeoHashLs);

        log.info("Given List size {}, Set size {}, Map size {}", givenGeoHashLs.size(), duplicateRemovedSet.size(), givenGeoHashMap.size());
    }

    private void findNewGeoHashes(List<String> givenGeoHashLs, List<String> calculatedGeoHashLs) {
        if (!calculatedGeoHashLs.containsAll(givenGeoHashLs)) {
            log.info("Not all givenGeoHashLs is found in calculatedGeoHashLs");
        }

        List<String> newlyCulculatedGeoHashes = new ArrayList<>();
        calculatedGeoHashLs.forEach(s -> {
            if (!givenGeoHashLs.contains(s)) {
                newlyCulculatedGeoHashes.add(s);
            }
        });

        log.info("Newly calculated and created geohashes are: {}", newlyCulculatedGeoHashes.size());
    }

    private List<String> getUserInputGeoHashList() {
        String[] givenGeoHashArray = {"w283tq85",
                                      "w283tnxg",
                                      "w283tnxg",
                                      "w283tnxg",
                                      "w283tnxg",
                                      "w283tnxe",
                                      "w283tnxe",
                                      "w283tnxd",
                                      "w283tnxd",
                                      "w283tnx9",
                                      "w283tnx3",
                                      "w283tnx3",
                                      "w283tnx2",
                                      "w283tnx2",
                                      "w283tnrr",
                                      "w283tnrr",
                                      "w283tnrq",
                                      "w283tnrq",
                                      "w283tnrm",
                                      "w283tnrm",
                                      "w283tnrk",
                                      "w283tnrk",
                                      "w283tnr7",
                                      "w283tnre",
                                      "w283tnrd",
                                      "w283tnrd",
                                      "w283tnrd",
                                      "w283tnrd",
                                      "w283tnrc",
                                      "w283tnrc",
                                      "w283tnrc",
                                      "w283tnrc",
                                      "w283tq21",
                                      "w283tq21",
                                      "w283tq21",
                                      "w283tq21",
                                      "w283tq23",
                                      "w283tq26",
                                      "w283tq26",
                                      "w283tq26",
                                      "w283tq2d",
                                      "w283tq2e",
                                      "w283tq2e",
                                      "w283tq2s",
                                      "w283tq2s",
                                      "w283tq2t",
                                      "w283tq2t",
                                      "w283tq2w",
                                      "w283tq2w",
                                      "w283tq2x",
                                      "w283tq2x",
                                      "w283tq88",
                                      "w283tq88",
                                      "w283tq89",
                                      "w283tq89",
                                      "w283tq89",
                                      "w283tq8d",
                                      "w283tq86",
                                      "w283tq87",
                                      "w283tq87",
                                      "w283tq87",
                                      "w283tq85",
                                      "w283tq85",
                                      "w283tq85"};

        return Arrays.asList(givenGeoHashArray);
    }

    private List<String> getCalculatedGeoHashList() {
        String[] calculatedGeoHashArray = {"w283tnr3",
                                           "w283tnr6",
                                           "w283tnr7",
                                           "w283tnr9",
                                           "w283tnrc",
                                           "w283tnrd",
                                           "w283tnre",
                                           "w283tnrf",
                                           "w283tnrg",
                                           "w283tnrk",
                                           "w283tnrm",
                                           "w283tnrq",
                                           "w283tnrr",
                                           "w283tnrs",
                                           "w283tnrt",
                                           "w283tnru",
                                           "w283tnrv",
                                           "w283tnrw",
                                           "w283tnrx",
                                           "w283tnry",
                                           "w283tnrz",
                                           "w283tnx2",
                                           "w283tnx3",
                                           "w283tnx6",
                                           "w283tnx7",
                                           "w283tnx8",
                                           "w283tnx9",
                                           "w283tnxb",
                                           "w283tnxc",
                                           "w283tnxd",
                                           "w283tnxe",
                                           "w283tnxf",
                                           "w283tnxg",
                                           "w283tq21",
                                           "w283tq23",
                                           "w283tq24",
                                           "w283tq25",
                                           "w283tq26",
                                           "w283tq27",
                                           "w283tq29",
                                           "w283tq2d",
                                           "w283tq2e",
                                           "w283tq2h",
                                           "w283tq2j",
                                           "w283tq2k",
                                           "w283tq2m",
                                           "w283tq2n",
                                           "w283tq2p",
                                           "w283tq2q",
                                           "w283tq2r",
                                           "w283tq2s",
                                           "w283tq2t",
                                           "w283tq2w",
                                           "w283tq2x",
                                           "w283tq80",
                                           "w283tq81",
                                           "w283tq82",
                                           "w283tq83",
                                           "w283tq84",
                                           "w283tq85",
                                           "w283tq86",
                                           "w283tq87",
                                           "w283tq88",
                                           "w283tq89",
                                           "w283tq8d",
                                           "w283tq8e"};

        log.info("Size of calculated geohashes: {}", calculatedGeoHashArray.length);
        return Arrays.asList(calculatedGeoHashArray);
    }

    @Disabled("Disabled until CustomerService is up!")
    @Test
    public void writeDeviceDetails() {
        List<ImeiTypeAllocationCodeEntity> imeiTypeAllocationCodeEntities = this.deviceDetailService.getAllDeviceDetail();
        ImeiTypeAllocationCodeModel imeiTypeAllocationCodeModel = new ImeiTypeAllocationCodeModel();

        for (ImeiTypeAllocationCodeEntity entity : imeiTypeAllocationCodeEntities) {
            imeiTypeAllocationCodeModel.addDevice(entity.getKey(), entity.getColumn1(), entity.getValue());
        }

        Map<String, DeviceDetail> deviceDetailMap = imeiTypeAllocationCodeModel.getDevice();

        //Converting the Object to JSONString
        try {
            String jsonString = mapper.writeValueAsString(deviceDetailMap);
            writeToFile(jsonString, "DeviceDetails");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Disabled("Disabled until CustomerService is up!")
    @Test
    public void writeDeviceDetailsToTable() {
        // Put the data to AC_ImeiTypeAllocationCode
        this.deviceDetailService.writeToDB("DeviceDetails.json");
        log.info("Completed!");
    }

    /**
     * Investigations without subjects but with result
     */
    @Test
    public void getAllConvoyInvestigationsWithoutSubjectButWithResult() {
        StringBuilder sb = new StringBuilder();
        analysisService.getAllInvestigationByType("CONVOY_ANALYSIS").forEach(entity -> {
            LocalDateTime createdDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.parseLong(entity.getColumn1())),
                                                                    TimeZone.getDefault().toZoneId());

            String status = null;
            try {
                status = analysisService.getInvestigationStatus(entity.getColumn2());
            } catch (Exception e) {

            }
            ConvoyAnalysisSubject convoyAnalysisSubject = analysisService.getSubject(entity.getColumn2());

            if (convoyAnalysisSubject == null) {
                sb.append(System.lineSeparator())
                  .append("createDate: ")
                  .append(formatter.format(createdDateTime))
                  .append(", ")
                  .append("investigation name: ")
                  .append(entity.getValue())
                  .append(", ")
                  .append("investigation status: ")
                  .append(status)
                  .append(", ")
                  .append("investigation id: ")
                  .append(entity.getColumn2())
                  .append(System.lineSeparator());
            }
        });

        log.info(sb.toString());
    }

    @Test
    public void getInvestigationStatusById() throws Exception {
        String investigationId = "e6eed310-f94e-11ed-878b-2c0da7f630d4";
        String status = analysisService.getInvestigationStatus(investigationId);;
        System.err.println(status);
    }

    @Test
    public void getAllConvoyInvestigations() throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss:SSS");
        StringBuilder finalBuilder = new StringBuilder();
        Set<String> wrongInvestigatioNames = new HashSet<>();
        analysisService.getAllInvestigationByType("CONVOY_ANALYSIS").forEach(entity -> {
            LocalDateTime createdDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.parseLong(entity.getColumn1())),
                                                                TimeZone.getDefault().toZoneId());

            String status = null;
            try {
                status = analysisService.getInvestigationStatus(entity.getColumn2());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            StringBuilder sb = new StringBuilder();
            sb.append(System.lineSeparator())
              .append("createDate: ")
              .append(formatter.format(createdDateTime))
              .append(", ")
              .append("investigation name: ")
              .append(entity.getValue())
              .append(", ")
              .append("investigation status: ")
              .append(status)
              .append(", ")
              .append("investigation id: ")
              .append(entity.getColumn2())
              .append(System.lineSeparator());

            ConvoyAnalysisSubject convoyAnalysisSubject = analysisService.getSubject(entity.getColumn2());

            if (convoyAnalysisSubject != null) {
                LocalDateTime subjectDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(convoyAnalysisSubject.getCreationDate()),
                                                                        TimeZone.getDefault().toZoneId());

                sb.append("Subject createdDate: ")
                  .append(formatter.format(subjectDateTime))
                  .append(System.lineSeparator());

                if (convoyAnalysisSubject.getObservedTargets() != null) {
                    convoyAnalysisSubject.getObservedTargets().forEach(subjectTarget -> {
                        sb.append("Target name: ").append(subjectTarget.getName()).append(", ");
                        subjectTarget.getTargetIdentities().forEach(targetSubjectIdentity -> {
                            sb.append("TargetIdentityId: ")
                              .append(targetSubjectIdentity.getIdentityId())
                              .append(", ")
                              .append("caption: ")
                              .append(targetSubjectIdentity.getIdentityCaption())
                              .append(", ")
                              .append("nature: ")
                              .append(targetSubjectIdentity.getIdentityNature())
                              .append(", ")
                              .append("serviceName: ")
                              .append(targetSubjectIdentity.getServiceName())
                              .append(System.lineSeparator());

                            String caption = targetSubjectIdentity.getIdentityCaption();
                            if (caption != null || !caption.isEmpty()) {
                                if (caption.equalsIgnoreCase("MSISDN") || caption.equalsIgnoreCase("IMSI") || caption.equalsIgnoreCase(
                                        "IMEI")) {
                                    wrongInvestigatioNames.add(entity.getValue()+ ";"+ entity.getColumn2());
                                }
                            }
                        });
                    });

                    convoyAnalysisSubject.getIdentities().forEach(subjectIdentity -> {
                        sb.append("IndividualIdentityId: ")
                          .append(subjectIdentity.getIdentityId())
                          .append(", ")
                          .append("caption: ")
                          .append(subjectIdentity.getIdentityCaption())
                          .append(", ")
                          .append("nature: ")
                          .append(subjectIdentity.getIdentityNature())
                          .append(", ")
                          .append("serviceName: ")
                          .append(subjectIdentity.getServiceName())
                          .append(System.lineSeparator());
                    });

                    finalBuilder.append(sb);
                }
            }

            ObservationResultEntity observationResultEntity =
                    analysisService.getObservationResult(entity.getColumn2());

            if (observationResultEntity != null) {
                System.err.println(entity.getValue());
                System.err.println(observationResultEntity.getValue());
            }
        });
        finalBuilder.append(System.lineSeparator());

        wrongInvestigatioNames.forEach(s -> finalBuilder.append("corrupted investigationName: ").append(s).append(System.lineSeparator()));
        log.info(finalBuilder.toString());
    }

    @Test
    public void getAllConvoySubject() throws Exception {
        this.analysisService.getAllConvoySubject();
    }

    @Test
    public void getAllInvestigationEvent() {
//        String investigationName = "mj2";
//        String investigationName = "ad_toy";
        String investigationName = "ad_toy_3";

        List<ConvoyObservationEvent> convoyObservationEvents = this.analysisService.getAllEventByInvestigationName(investigationName);
       log.info("observationResults: {}", convoyObservationEvents.size());
    }

    private String getProximityInvestigationByName(String investigationName) throws JsonProcessingException {
        String investigationId = "";
        System.err.println("InvestigationName: " + investigationName);
        for (ProximityAnalysisInvestigationEntity entity : this.proximityAnalysisInvestigationRepository.findAll()) {
            PaInvestigationModel paInvestigationModel = mapper.readValue(entity.getValue(), PaInvestigationModel.class);
            if (paInvestigationModel.getInvestigationName().equalsIgnoreCase(investigationName)) {
                System.err.println("[ProximityAnalysis_Investigation](key)InvestigationId: " + entity.getKey() + ", name: " + investigationName);
                return entity.getKey();
            }
        }
        return null;
    }

    @Test
    public void getInvestigationByProcessId() throws Exception {
        String processId = "7f5544f0-bf10-11ed-9b32-2c0da7f630d4";
        Set<InvestigationSearchEntity> searchEntities = new HashSet<>();
        searchEntities.addAll(analysisService.getAllInvestigationByType("CDR")) ;
        searchEntities.addAll(analysisService.getAllInvestigationByType("LDR")) ;
        searchEntities.addAll(analysisService.getAllInvestigationByType("IPDR")) ;

        for (InvestigationSearchEntity search : searchEntities) {
            if (search.getValue().equalsIgnoreCase(processId) || search.getColumn2().equalsIgnoreCase(processId)) {
                System.err.println("[InvestigationSearch]([value = investigationId] : " + processId + ")([column2 = processId] : " + processId);
            }
        }

        InvestigationManagement investigationManagement = analysisService.getInvestigationByKey(processId);

        logActionTrace(processId);

        System.err.println("\n[InvestigationManagement_v2] for key: "
                                   + processId + System.lineSeparator()
                                   + InvestigationManagement.getHeader()
                                   + System.lineSeparator()
                                   + investigationManagement.toString());

        logObservationMetadata(investigationManagement.getObservationList());
        logFileMetadata(investigationManagement.getObservationList());


    }

    @Test
    public void getInvestigation() {
        List<InvestigationSearchEntity> entities = analysisService.getAllInvestigations();

        StringBuilder sb = new StringBuilder("key;column1;column2;value\n");
        for (InvestigationSearchEntity investigationSearchEntity : entities) {
            sb.append(investigationSearchEntity.getKey()).append(";");
            sb.append(investigationSearchEntity.getColumn1()).append(";");
            sb.append(investigationSearchEntity.getColumn2()).append(";");
            sb.append(investigationSearchEntity.getValue()).append(System.lineSeparator());
        }

        System.err.println(sb);
    }

    @Test
    public void getEbaDetails() {
        String investigationName = "test5_eba";

        // "InvestigationSearch" table
        InvestigationSearchEntity investigation = getConvoyEbaInvestigationIdByInvestigationName(investigationName);
        if (investigation == null) {
            System.err.println("Investigation " + investigationName + " not found!");
            return;
        }

        String investigationId = investigation.getColumn2();

        System.err.println(investigation.getKey() + " InvestigationId: " + investigationId + System.lineSeparator());

        // "InvestigationManagement_v2" table
        List<String> observationIds = getEbaObservationIds(investigationId);
        System.err.println(investigation.getKey() + " observationIds: " + observationIds);

        if (observationIds == null) return;
        for (String observationId: observationIds) {
            // get value from "InvestigationSearch" table
            getProcessIdDetailsByInvestigationId(observationId);
        }
    }

    @Test
    public void getInvestigationManagement() {
        List<InvestigationManagementEntity> investigationManagementEntities = analysisService.getAllInvestigationManagement();

        StringBuilder sb = new StringBuilder(InvestigationManagementEntity.getHeader()).append(System.lineSeparator());
        for (InvestigationManagementEntity entity: investigationManagementEntities) {
            sb.append(entity.getKey()).append(";").append(entity.getColumn1()).append(";").append(entity.getValue()).append(System.lineSeparator());
        }

        System.err.println(sb);
    }

    @Test
    public void getConvoyInvestigationByName() throws Exception {
        String investigationName = "test3_eba";

        InvestigationSearchEntity investigation = getConvoyEbaInvestigationIdByInvestigationName(investigationName);
        if (investigation == null) {
            System.err.println("Investigation " + investigationName + " not found!");
            return;
        }

        String investigationId = investigation.getColumn2();

        System.err.println("Convoy InvestigationId: " + investigationId + System.lineSeparator());

        printInvestigationManagementWithKey(Arrays.asList(investigationId));

        getProcessIdDetailsByInvestigationId(investigationId);
    }

    private void printInvestigationManagementWithKey(List<String> keys){
        StringBuilder sb = new StringBuilder();
        Map<String, List<String>> processFiles = new HashMap<>();
        List<String> types = Arrays.asList("CDR", "LDR", "IPDR", "cdr", "ldr", "ipdr");
        boolean found = false;
        for (String key: keys) {
            sb.append("Additional investigation information for key: ").append(key).append(System.lineSeparator());
            InvestigationManagement investigationManagement = analysisService.getInvestigationManagementByKey(key);
            sb.append(InvestigationManagement.HEADER).append(System.lineSeparator());
            sb.append(investigationManagement).append(System.lineSeparator());

            String type = investigationManagement.getInvestigationType();
            if (types.contains(type)) processFiles.put(key, investigationManagement.getObservationList());
            found = true;
        }

        if (found) System.err.println(sb);

        StringBuilder processFileSb = new StringBuilder();
        boolean foundFiles = false;
        if (!processFiles.isEmpty()) {
            for (String processId : processFiles.keySet()) {
                foundFiles = logFileMetadata(processFileSb, processId, processFiles.get(processId));
            }
        }

        if (foundFiles) System.err.println(processFileSb);
    }

    private List<String> getEbaObservationIds(String investigationId) {
        StringBuilder sb = new StringBuilder();
        sb.append("Additional EBA investigation information for key: ").append(investigationId).append(System.lineSeparator());
        InvestigationManagement investigationManagement = analysisService.getInvestigationManagementByKey(investigationId);
        sb.append(InvestigationManagement.HEADER).append(System.lineSeparator());
        sb.append(investigationManagement).append(System.lineSeparator());
        System.err.println(sb);
        return investigationManagement.getObservationList();
    }

    private InvestigationSearchEntity getConvoyEbaInvestigationIdByInvestigationName(String investigationName) {
        StringBuilder sb = new StringBuilder("investigation name: ")
                .append(investigationName)
                .append(System.lineSeparator())
                .append(System.lineSeparator());
        sb.append("Table: ").append("InvestigationSearch").append(System.lineSeparator());
        sb.append("key;column1;column2;value\n");

        // Get investigation from 'InvestigationSearch'
        InvestigationSearchEntity investigation = analysisService.getInvestigationByName(investigationName);

        if (investigation == null) return null;

        if (investigation != null) {
            sb.append(investigation.getKey()).append(";");
            sb.append(investigation.getColumn1()).append(";");
            sb.append(investigation.getColumn2()).append(";");
            sb.append(investigation.getValue()).append(System.lineSeparator());
        }

        System.err.println(sb);

        return investigation;
    }

    private void getProcessIdDetailsByInvestigationId(String investigationId) {
        List<InvestigationSearchEntity> entities = analysisService.getProcessIdByConvoyInvestigationId(investigationId);
        List<String> processIds = new ArrayList<>();
        StringBuilder processIdSb = new StringBuilder();
        processIdSb.append("Table: InvestigationSearch, ProcessIds which belongs to investigationId: ").append(investigationId).append(System.lineSeparator());
        processIdSb.append("type;processId").append(System.lineSeparator());
        for (InvestigationSearchEntity entity : entities) {
            processIds.add(entity.getColumn2());
            processIdSb.append(entity.getKey()).append(";").append(entity.getColumn2()).append(System.lineSeparator());
        }
        System.err.println(processIdSb);

        // InvestigationManagement_v2
        printInvestigationManagementWithKey(processIds);

        // ImportProcessMetadata
        printInputProcessMetadata(processIds);
    }

    private void printInputProcessMetadata(List<String> keys) {
        for (String key : keys) {
            System.err.println("Table: ImportProcessMetadata, by id: " + key + System.lineSeparator() + analysisService.getImportProcessMetadata(key));
            logActionTrace(key);
        }
    }

    @Test
    public void getProximityInvestigationByName() throws Exception {
//        String investigationName  = "202200907_4";
        String investigationName  = "20220928_06";
        String investigationId  = getProximityInvestigationByName(investigationName);
        if (investigationId == null) {
            System.err.println("Investigation " + investigationName + " not found!");
            return;
        }

        Set<String> processIds = analysisService.getProcessId(investigationId);
        if (processIds.isEmpty()) return;

        Set<InvestigationSearchEntity> searchEntities = new HashSet<>();
        searchEntities.addAll(analysisService.getAllInvestigationByType("CDR")) ;
        searchEntities.addAll(analysisService.getAllInvestigationByType("LDR")) ;
        searchEntities.addAll(analysisService.getAllInvestigationByType("IPDR")) ;

        for (String processId : processIds){
            System.err.println("InvestigationId: " + investigationId + ", ProcessId: " + processId);
            for (InvestigationSearchEntity search : searchEntities) {
                if (search.getValue().equalsIgnoreCase(investigationId) && search.getColumn2().equalsIgnoreCase(processId)) {
                    System.err.println("[InvestigationSearch]([value = investigationId] : " + investigationId + ")([column2 = processId] : " + processId);
                }
            }

            System.err.println("\n[InvestigationManagement_v2] for key: " + investigationId + System.lineSeparator() + analysisService.getInvestigationByKey(investigationId));

            InvestigationManagement investigationManagement = analysisService.getInvestigationByKey(processId);

            System.err.println("\n[InvestigationManagement_v2] for key: " + processId + System.lineSeparator() +investigationManagement.toString());

            logObservationMetadata(investigationManagement.getObservationList());
            logFileMetadata(investigationManagement.getObservationList());
            logInvestigationMetadataEx(processId);
            logActionTrace(processId);
        }
    }

    private void logActionTrace(String processId) {
        List<ActionTraceEntity> actionTraceEntities = actionTraceRepository.findByKey(processId);
        StringBuilder sb = new StringBuilder();
        sb.append("\n[ActionTrace] for key: ").append(processId).append(System.lineSeparator());
        for (ActionTraceEntity actionTrace: actionTraceEntities) {
            sb.append("column2: ").append(actionTrace.getColumn2()).append(System.lineSeparator());
        }
        System.err.println(sb);
    }

    private boolean logFileMetadata(StringBuilder sb, String processId, List<String> observationList) {
        if (observationList == null || observationList.isEmpty()) return false;
        boolean found = false;
        sb.append("\n[FileMetadata] for processId: ").append(processId).append("keys: ").append(observationList).append(System.lineSeparator());
        sb.append(FileMetadataModel.HEADER).append(System.lineSeparator());

        for (String fileId: observationList) {
            FileMetadataModel model = fileMetadataService.getByKey(fileId);
            found= true;
            sb.append(model.toString()).append(System.lineSeparator());
        }
        return found;
    }

    private void logFileMetadata(List<String> observationList) {
        if (observationList == null || observationList.isEmpty()) return;
        boolean found = false;
        StringBuilder sb = new StringBuilder();
        sb.append("\n[FileMetadata] for keys: ").append(observationList).append(System.lineSeparator());
        for (FileMetadataEntity ob : fileMetadataRepository.findAll()) {
            if (observationList.contains(ob.getKey())) {
                found= true;
                sb.append(ob.getKey()).append(", ").append(ob.getColumn1()).append(", ").append(ob.getValue()).append(System.lineSeparator());
            }
        }
        if (found) System.err.println(sb);
    }

    private void logObservationMetadata(List<String> observationList) {
        if (observationList == null || observationList.isEmpty()) return;
        StringBuilder sb = new StringBuilder();
        sb.append("\n[ObservationMetadata_v2] for keys: ").append(observationList).append(System.lineSeparator());
        boolean found = false;
        for (ObservationMetadataEntity ob : observationMetadataRepository.findAll()) {
            if (observationList.contains(ob.getKey())) {
                found= true;
                sb.append(ob.getKey()).append(", ").append(ob.getColumn1()).append(", ").append(ob.getValue()).append(System.lineSeparator());
            }
        }
        if (found) System.err.println(sb);
    }

    private void logInvestigationMetadataEx(String processId) {
        StringBuilder sb;
        List<InvestigationMetadataExEntity> investigationMetadataExEntities = investigationMetadataExRepository.findByKey(processId);

        sb = new StringBuilder();
        sb.append("\n[InvestigationMetadataEx]").append("key: ").append(processId).append(System.lineSeparator());
        for (InvestigationMetadataExEntity entity: investigationMetadataExEntities) {
            sb.append(entity.getColumn1()).append(" : ").append(entity.getValue()).append(System.lineSeparator());
        }

        System.err.println(sb);
    }

    @Test
    public void getAllInvestigationEventByName() {
//        String investigationName = "mj2";
        String investigationName = "ad_toy_3";

        List<ConvoyObservationEvent> convoyObservationEvents = this.analysisService.getAllEventByInvestigationName(investigationName);
        log.info("observationResults: {}", convoyObservationEvents.size());
    }

    @Test
    public void getAllInvestigationEvents(){
        this.analysisService.getAllEventByAllInvestigation();
    }

    @Test
    public void getAllCgiMsisdnActivity(){
        this.acCgiMsisdnActivityRepository.findByKey("219-10-1280-40301");
    }
}
