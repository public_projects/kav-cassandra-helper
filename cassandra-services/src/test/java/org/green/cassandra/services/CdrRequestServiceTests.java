package org.green.cassandra.services;

import org.green.cassandra.services.core.impl.CdrRequestServiceImpl;
import org.green.cassandra.services.core.model.dataretention.AcCdrRequestStatusModel;
import org.green.cassandra.services.util.DateConvertorUtil;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@SpringBootTest
@ActiveProfiles("LOCAL")
//@ActiveProfiles("DCNG2")
public class CdrRequestServiceTests {
    private final static Logger logger = LoggerFactory.getLogger(CdrRequestServiceTests.class);

    @Autowired
    private CdrRequestServiceImpl cdrRequestService;

    @Test
    public void testGetAllRequests() {
        List<AcCdrRequestStatusModel> results = cdrRequestService.getAllCdrRequestStatuses();
        logger.info("[AC_CdrRequestStatus] has {} records", results.size());

        StringBuilder sb = new StringBuilder();
        sb.append(System.lineSeparator())
          .append(System.lineSeparator())
          .append(AcCdrRequestStatusModel.HEADERS)
          .append(System.lineSeparator());

        DateConvertorUtil dateConvertorUtil = DateConvertorUtil.of("yyyy.MM.dd_HH.mm.ss");
        String fileName = "AC_CdrRequestStatus_" + dateConvertorUtil.convertEpoch(System.currentTimeMillis()) + ".txt";
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(fileName);
            fileWriter.write(AcCdrRequestStatusModel.HEADERS + System.lineSeparator());

            for (AcCdrRequestStatusModel result : results) {
                if (result.getId().equals("147346021504857") && result.getType().equals("MSISDN")) {
                    fileWriter.write(result + System.lineSeparator());
                    long epochTime = Long.parseLong(result.getColumnValueMap().get(AcCdrRequestStatusModel.REQUEST_END));
                    System.err.println(epochTime + "(" + DateConvertorUtil.getDateTime(epochTime)+ ") = " + DateConvertorUtil.getYearWeek(epochTime));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileWriter != null) {
                    fileWriter.flush();
                    fileWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        logger.info("END ... {}", fileName);
    }
}
