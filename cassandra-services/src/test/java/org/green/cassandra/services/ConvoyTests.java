package org.green.cassandra.services;

import org.green.cassandra.entity.GeoIdentityMovementActivityEntity;
import org.green.cassandra.entity.geo.GeoIdentityMovementActivityWeekBucketEntity;
import org.green.cassandra.entity.investigation.InvestigationSearchEntity;
import org.green.cassandra.repository.geo.GeoIdentityMovementActivityRepository;
import org.green.cassandra.repository.geo.GeoIdentityMovementActivityWeekBucketRepository;
import org.green.cassandra.services.core.AnalysisService;
import org.green.cassandra.services.core.model.convoy.ConvoyAnalysisSubject;
import org.javatuples.Pair;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.text.DecimalFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@ActiveProfiles("LOCAL")
//@ActiveProfiles("DCNG2")
public class ConvoyTests {
    private final static Logger logger = LoggerFactory.getLogger(ConvoyTests.class);

    @Autowired
    private GeoIdentityMovementActivityWeekBucketRepository geoIdentityMovementActivityWeekBucketRepository;

    @Autowired
    private GeoIdentityMovementActivityRepository geoIdentityMovementActivityRepository;

    @Autowired
    private AnalysisService analysisService;

    @Test
    public void getSubject() {
        List<InvestigationSearchEntity> entities = analysisService.getAllInvestigationByType("CONVOY_ANALYSIS");

        StringBuilder sb = new StringBuilder("key;column1;column2;value\n");
        for (InvestigationSearchEntity investigationSearchEntity : entities) {
            String investigationId = investigationSearchEntity.getColumn2();
            sb.append(investigationSearchEntity.getKey()).append(";");
            sb.append(investigationId).append(";");
            sb.append(investigationSearchEntity.getColumn2()).append(";");
            sb.append(investigationSearchEntity.getValue()).append(System.lineSeparator());

            ConvoyAnalysisSubject subject = analysisService.getSubject(investigationId);
            if (subject == null) continue;
            System.err.println(investigationSearchEntity.getValue() + ": " + subject);
        }

        System.err.println(sb);

        String investigationId = "97ac2c60-f94b-11ed-a652-0242ac11000d";
        ConvoyAnalysisSubject subject = analysisService.getSubject(investigationId);
        System.err.println(subject);
    }

    @Test
    public void getMovements() {
        StringBuilder output = new StringBuilder("\n");

        List<String> identityIds = new ArrayList<>();
        identityIds.add(encodeId("361275444688708", "MSISDN"));

        LocalDateTime from = LocalDateTime.of(2023, 01, 27, 23, 59, 59);
        LocalDateTime to = LocalDateTime.of(2023, 07, 21, 23, 59, 59);

        output.append("from: ").append(from).append(", to: ").append(to).append("\n");
        for (String identityId : identityIds) {
            List<GeoIdentityMovementActivityWeekBucketEntity> bucketEntityList =
                    geoIdentityMovementActivityWeekBucketRepository.findByKey(identityId);
            output.append("Identity: ").append(identityId).append("\n\n");
            LocalDateTime recordStart = null;
            LocalDateTime recordEnd = null;

            List<GeoIdentityMovementActivityWeekBucketEntity> validLs = new ArrayList<>();
            for (GeoIdentityMovementActivityWeekBucketEntity entity : bucketEntityList) {
                String yearWeekStr = entity.getColumn1();
                String value = entity.getValue();
                Pair<Integer, Integer> yearWeek = getYearAndWeekFromInput(yearWeekStr);
                recordStart = getStartDateOfWeek(yearWeek.getValue0(), yearWeek.getValue1());
                recordEnd = getEndDateOfWeek(recordStart);

                output.append("[" + recordStart + " -  " + recordEnd + "], " + yearWeekStr + ", records: " + value).append("\n");
                if (to.isBefore(recordStart) || to.isEqual(recordStart)) continue;

                if (from.isBefore(recordStart) || to.isAfter(recordStart)) {
                    validLs.add(entity);
                    continue;
                }

                if (from.isBefore(recordEnd) || to.isAfter(recordEnd)) {
                    validLs.add(entity);
                }
            }

            output.append("\n");

            for (GeoIdentityMovementActivityWeekBucketEntity entity : validLs) {
                Long yearWeekStr = Long.valueOf(entity.getColumn1());
                String value = entity.getValue();

                output.append("Fetch key: " + identityId + ", key2: " + yearWeekStr).append("\n");

                List<GeoIdentityMovementActivityEntity> movementActivityEntities =
                        geoIdentityMovementActivityRepository.findByKeyAndKey2(identityId, yearWeekStr);

                output.append("selected [" + recordStart + " -  " + recordEnd + "], " + yearWeekStr + ", expected records: " + value + ", found: " + movementActivityEntities.size()).append("\n\n");
            }
        }

        logger.info("\n{}", output);
    }

    private String encodeId(String id, String type) {
        int typeLength = type.length();
        int idLength = id.length();

        DecimalFormat df = new DecimalFormat("000");
        String typeFormat = df.format(typeLength);
        String idFormat = df.format(idLength);

        return typeFormat + "S" + type.toUpperCase() + idFormat + "S" + id;
    }

    public static LocalDateTime getStartDateOfWeek(int year, int weekNumber) {
        return LocalDate.of(year, 1, 1)
                        .with(TemporalAdjusters.firstDayOfYear())
                        .plusWeeks(weekNumber - 1)
                        .with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).atStartOfDay();
    }

    public static LocalDateTime getEndDateOfWeek(LocalDateTime startDate) {
        return LocalDateTime.of(startDate.plusDays(6).toLocalDate(), LocalTime.MAX);
    }

    public static Pair<Integer, Integer> getYearAndWeekFromInput(String input) {
        int year = Integer.parseInt(input.substring(0, 4));
        int week = Integer.parseInt(input.substring(input.length() - 2));
        return Pair.with(year, week);
    }
}
